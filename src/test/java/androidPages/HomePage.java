package androidPages;

import java.io.IOException;

import com.Tracfone.driverInstance.DTAFCommandBase_Android;
import com.Tracfone.driverInstance.DTAFCommandBase_iOS;
import com.Tracfone.extent.ExtentReporter;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.SimplyMobile_androidObjects;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;


public class HomePage extends DTAFCommandBase_Android{

	public DeviceTypePage _deviceTypePage;
	public ActivationTypePage _activationType;
	public NewPhoneNumberPage _newPhoneNumberPage;
	public ServicePlanPage servicePlanPage;
	
	ExtentReporter _extentReport = new ExtentReporter();
	public HomePage(AndroidDriver<MobileElement> androidDriver, String Platform)throws IOException, InterruptedException 
	{
		super(androidDriver, Platform);
		this.androidDriver = androidDriver;
		System.out.println("Driver in Base Class Android is" +androidDriver);
	}

//user clicks on menuList in home screen	
public void clickHomeScreenMenuList()
{		
	Wait(30);
	checkElementPresent(SimplyMobile_androidObjects.logInPageTitle);
	 String loginPageTitle = findElement(SimplyMobile_androidObjects.logInPageTitle).getText();
	 System.out.println("Login Page Text--->>>"+loginPageTitle);
	 loginPageTitle.contains("Log In");
	 ExtentTestManager.getTest().log(Status.INFO,"Validating the Login Screen expected to contain found" + loginPageTitle);
		click(SimplyMobile_androidObjects.menuButton);	 
		ExtentTestManager.getTest().log(Status.PASS, "Menu button is clicked");
}

//user clicks on Add Device or Line option from menuList
public void clickAddDevice() {
	click(SimplyMobile_androidObjects.addDevice);
	ExtentTestManager.getTest().log(Status.PASS, "Selected AddDevice option from menuList ");
}

//user clicks on ActivateDevice in homeScreen
public void clickActivateDevice() {
	checkElementPresent(SimplyMobile_androidObjects.activateDevice);
	click(SimplyMobile_androidObjects.activateDevice);
	ExtentTestManager.getTest().log(Status.PASS, "Activate Device option is clicked");
}


public void enterSimCardNumber() {
	click(SimplyMobile_androidObjects.simCardField);
	Wait(5);
	type(SimplyMobile_androidObjects.simCardField, "8901260321162031425");
	
}

public void checkTermsAndConditions() {
	click(SimplyMobile_androidObjects.checkBox);
	
}

public void sMPContinueButton() {
	click(SimplyMobile_androidObjects.simCardContinue);
	
}

public void clickNewPhoneNumber() {
	click(SimplyMobile_androidObjects.newPhoneNumber);
	
}

public void enterZipCode() {
	click(SimplyMobile_androidObjects.zipCodeField);
	Wait(5);
	type(SimplyMobile_androidObjects.zipCodeField, "32718");
	
}

public void zipCodeContinue() {
	click(SimplyMobile_androidObjects.zipCodeContinue);
	
}

public void clickServicePlanWithPin() {
	click(SimplyMobile_androidObjects.servicePlanWithPin);
	
}

public void enterServicePin() {
	click(SimplyMobile_androidObjects.servicePinField);
	Wait(5);
	type(SimplyMobile_androidObjects.servicePinField, "999999938641637");
	
}

public void servicePinContinue(){
	click(SimplyMobile_androidObjects.servicePinContinue);
}

public void clickCreateAccount(){
	click(SimplyMobile_androidObjects.createAccount);
}


public void enterEmail(){
	click(SimplyMobile_androidObjects.enterEmail);
	Wait(5);
	type(SimplyMobile_androidObjects.enterEmail, "100000021028211@sit1.com");
}

public void confirmEmail(){
	click(SimplyMobile_androidObjects.confirmEmail);
	Wait(5);
	type(SimplyMobile_androidObjects.confirmEmail, "100000021028211@sit1.com");
}

public void enterPassword(){
	click(SimplyMobile_androidObjects.password);
	Wait(5);
	type(SimplyMobile_androidObjects.password, "123456");
}

public void confirmPassword(){
	click(SimplyMobile_androidObjects.confirmPassword);
	Wait(5);
	type(SimplyMobile_androidObjects.confirmPassword, "123456");
}


public void clickOnActivateDevice() {
	Wait(30);
	clickHomeScreenMenuList();
	Wait(10);
	clickAddDevice();
	clickActivateDevice();
	
}


	  
		
}