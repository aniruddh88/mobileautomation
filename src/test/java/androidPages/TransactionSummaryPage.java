package androidPages;

import java.io.IOException;

import com.Tracfone.driverInstance.DTAFCommandBase_Android;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.SimplyMobile_androidObjects;
import com.aventstack.extentreports.Status;


import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class TransactionSummaryPage extends DTAFCommandBase_Android{

	public TransactionSummaryPage(AndroidDriver<MobileElement> androidDriver, String Platform)
			throws IOException, InterruptedException {
		super(androidDriver, Platform);
		this.androidDriver = androidDriver;
		System.out.println("Driver in Base Class Android is" + androidDriver);
	}

	public void doneButton() {
		Wait(30);
		checkElementPresent(SimplyMobile_androidObjects.transactionSummaryTitle);
		 String transactionSummaryPageTitle = findElement(SimplyMobile_androidObjects.transactionSummaryTitle).getText();
		 System.out.println("Transaction Summary Page Text--->>>"+transactionSummaryPageTitle);
		 transactionSummaryPageTitle.contains("Transaction Summary");
		 ExtentTestManager.getTest().log(Status.INFO,"Validating the Transaction Summary Screen expected to contain found" + transactionSummaryPageTitle);
		click(SimplyMobile_androidObjects.doneButton);
		ExtentTestManager.getTest().log(Status.PASS, "Done button is clicked");
	}

	
}
