package androidPages;

import java.io.IOException;

import com.Tracfone.driverInstance.DTAFCommandBase_Android;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.SimplyMobile_androidObjects;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class ActivationTypePage extends DTAFCommandBase_Android{

	public ActivationTypePage(AndroidDriver<MobileElement> androidDriver, String Platform)
			throws IOException, InterruptedException {
		super(androidDriver, Platform);
		this.androidDriver = androidDriver;
		System.out.println("Driver in Base Class Android is" +androidDriver);
	}
	
	//user clicks on NewPhoneNumber option
	public void clickNewPhoneNumber() {
		Wait(30);
		checkElementPresent(SimplyMobile_androidObjects.activationTypeTitle);
		 String activationTypePageTitle = findElement(SimplyMobile_androidObjects.activationTypeTitle).getText();
		 System.out.println("Activation Type Page Text--->>>"+activationTypePageTitle);
		 activationTypePageTitle.contains("Activation Type");
		 ExtentTestManager.getTest().log(Status.INFO,"Validating the Activation Type Screen expected to contain found" + activationTypePageTitle);
		click(SimplyMobile_androidObjects.newPhoneNumber);
		ExtentTestManager.getTest().log(Status.PASS, "Entered the phone number");
	}

}
