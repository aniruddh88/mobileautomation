package androidPages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;

import com.Tracfone.driverInstance.DTAFCommandBase_Android;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.SimplyMobile_androidObjects;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class CreateAccountPage extends DTAFCommandBase_Android {

	public CreateAccountPage(AndroidDriver<MobileElement> androidDriver, String Platform)
			throws IOException, InterruptedException {
		super(androidDriver, Platform);
		this.androidDriver = androidDriver;
		System.out.println("Driver in Base Class Android is" + androidDriver);
	}

	public void enterEmail() {
		click(SimplyMobile_androidObjects.enterEmail);
		Wait(5);
		type(SimplyMobile_androidObjects.enterEmail, "260321225671458@sit1.com");
		ExtentTestManager.getTest().log(Status.PASS, "Email id has been entered");
	}

	public void confirmEmail() {
		Wait(10);
		click(SimplyMobile_androidObjects.confirmEmail);
		Wait(5);
		//click(SimplyMobile_androidObjects.closeButton);
		//Wait(5);
		click(SimplyMobile_androidObjects.confirmEmail);
		Wait(5);
		type(SimplyMobile_androidObjects.confirmEmail, "260321225671458@sit1.com");
	}

	public void enterPassword() {
		click(SimplyMobile_androidObjects.password);
		Wait(5);
		type(SimplyMobile_androidObjects.password, "123456");
		ExtentTestManager.getTest().log(Status.PASS, "Password has been entered");
	}

	public void confirmPassword() {
		click(SimplyMobile_androidObjects.confirmPassword);
		Wait(5);
		type(SimplyMobile_androidObjects.confirmPassword, "123456");
	}

	public void enterPin() {
		click(SimplyMobile_androidObjects.pin);
		Wait(5);
		type(SimplyMobile_androidObjects.pin, "1234");
		ExtentTestManager.getTest().log(Status.PASS, "Pin hs been enetered");
	}

	public void confirmPin() {
		click(SimplyMobile_androidObjects.confirmPin);
		Wait(5);
		type(SimplyMobile_androidObjects.confirmPin, "1234");
	}

	public void enterDateOfBirth() throws Exception {
		Wait(30);
		hideKeyboard();
		Wait(5);
		click(SimplyMobile_androidObjects.dateOfBirth);
		Wait(5);
		type(SimplyMobile_androidObjects.dateOfBirth, "04-23-2000");
		Wait(10);
		ExtentTestManager.getTest().log(Status.PASS, "Date of birth value has been entered");
	}

	public void enterDataForAccountCreation() throws Exception {
		Wait(15);
		checkElementPresent(SimplyMobile_androidObjects.createAccountTitle);
		 String createAccountPageTitle = findElement(SimplyMobile_androidObjects.createAccountTitle).getText();
		 System.out.println("Create Account Page Text--->>>"+createAccountPageTitle);
		 createAccountPageTitle.contains("Create Account");
		 ExtentTestManager.getTest().log(Status.INFO,"Validating the Create Account Screen expected to contain found" + createAccountPageTitle);
		enterEmail();
		confirmEmail();
		enterPassword();
		confirmPassword();
		enterPin();
		confirmPin();
		enterDateOfBirth();
		//saveAccount();
		//okButtonAccountCreated();
	}

	public void saveAccount() {
		hideKeyboard();
		Wait(5);
		click(SimplyMobile_androidObjects.saveAccount);
		ExtentTestManager.getTest().log(Status.PASS, "Clicked on save account option");

	}

	public void okButtonAccountCreated() {
		Wait(5);
		click(SimplyMobile_androidObjects.okButtonAccountCreated);
		ExtentTestManager.getTest().log(Status.PASS, "Clicked on ok button");
	}

}
