package androidPages;

import java.io.IOException;

import com.Tracfone.driverInstance.DTAFCommandBase_Android;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.SimplyMobile_androidObjects;
import com.aventstack.extentreports.Status;


import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class DeviceConfigurationPage extends DTAFCommandBase_Android{
	
	public DeviceConfigurationPage(AndroidDriver<MobileElement> androidDriver, String Platform)
			throws IOException, InterruptedException {
		super(androidDriver, Platform);
		this.androidDriver = androidDriver;
		System.out.println("Driver in Base Class Android is" +androidDriver);
	}
	
	public void continueDC() {
		Wait(120);
		checkElementPresent(SimplyMobile_androidObjects.deviceConfigurationTitle);
		 String deviceConfigurationPageTitle = findElement(SimplyMobile_androidObjects.deviceConfigurationTitle).getText();
		 System.out.println("Device Configuration Page Text--->>>"+deviceConfigurationPageTitle);
		 deviceConfigurationPageTitle.contains("DEVICE CONFIGURATION");
		 ExtentTestManager.getTest().log(Status.INFO,"Validating the Device Configuration Screen expected to contain found" + deviceConfigurationPageTitle);
		click(SimplyMobile_androidObjects.continueButtonDC);
		ExtentTestManager.getTest().log(Status.PASS, "Clicked on continue button");
	}
	
	public void doneButton() {
		click(SimplyMobile_androidObjects.doneButton);
	}

}
