package androidPages;

import java.io.IOException;

import com.Tracfone.driverInstance.DTAFCommandBase_Android;
import com.Tracfone.extent.ExtentReporter;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.SimplyMobile_androidObjects;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class LoginPage extends com.Tracfone.driverInstance.DTAFCommandBase_Android{

	
	ExtentReporter _extentReport = new ExtentReporter();
	public LoginPage(AndroidDriver<MobileElement> androidDriver, String Platform)throws IOException, InterruptedException 
	{
		super(androidDriver, Platform);
		this.androidDriver = androidDriver;
		System.out.println("Driver in Base Class Android is" +androidDriver);
	}
	
	/*******************************************************
	 * Login Functionality
	 * @param Username
	 * @param Password
	 *******************************************************/
	
	public void login(String Username,String Password)
	{
		
		Wait(50); 
		
		
		ExtentTestManager.getTest().log(Status.PASS, "Successfully Logged in to the Application");
	}
	
}