package androidPages;

import java.io.IOException;
import java.util.List;

import com.Tracfone.driverInstance.DTAFCommandBase_Android;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.SimplyMobile_androidObjects;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class MyAccountSummaryPage extends DTAFCommandBase_Android {

	public MyAccountSummaryPage(AndroidDriver<MobileElement> androidDriver, String Platform)
			throws IOException, InterruptedException {
		super(androidDriver, Platform);
		this.androidDriver = androidDriver;
		System.out.println("Driver in Base Class Android is" + androidDriver);
	}
	
	public void verifyMyAccountSummaryPageTitle() {
		Wait(30);
		checkElementPresent(SimplyMobile_androidObjects.myAccountSummaryTitle);
		 String transactionSummaryPageTitle = findElement(SimplyMobile_androidObjects.myAccountSummaryTitle).getText();
		 System.out.println("My Account Summary Page Text--->>>"+transactionSummaryPageTitle);
		 transactionSummaryPageTitle.contains("My Account Summary");
		 ExtentTestManager.getTest().log(Status.INFO,"Validating the My Account Summary Screen expected to contain found" + transactionSummaryPageTitle);
	}
	
}
