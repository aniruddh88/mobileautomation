package androidPages;

import java.io.IOException;

import com.Tracfone.driverInstance.DTAFCommandBase_Android;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.SimplyMobile_androidObjects;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class DeviceTypePage extends DTAFCommandBase_Android{

	public DeviceTypePage(AndroidDriver<MobileElement> androidDriver, String Platform)
			throws IOException, InterruptedException {
		super(androidDriver, Platform);
		this.androidDriver = androidDriver;
		System.out.println("Driver in Base Class Android is" +androidDriver);
	}
	
	//user select deviceType as SimpleMobilePhone
	public void selectDeviceType() {
		
		checkElementPresent(SimplyMobile_androidObjects.devicePageHeading);
		 String devicePageHeading = findElement(SimplyMobile_androidObjects.devicePageHeading).getText();
		 System.out.println("Device Page Text--->>>"+devicePageHeading);
		 devicePageHeading.contains("Device Type");
		 ExtentTestManager.getTest().log(Status.INFO,"Validating the Device Page Title expected to contain found" + devicePageHeading);
		 
		click(SimplyMobile_androidObjects.bringOwnDevice);
		ExtentTestManager.getTest().log(Status.PASS, "Bring Own Device option is clicked");
		
	}
	
	//user select deviceType as SimpleMobilePhone and enter the simCardNumber
	public void enterSimCardNumber() {
		click(SimplyMobile_androidObjects.simCardField);
		Wait(5);
		//get the simCarNumber by running the below query 
		/*set serveroutput on
		  Begin
		  dbms_output.put_line ('ESN :'|| sa.get_test_esn('SMSAG965U1GP5'));
		  --Enter part number SMSAG965U1GP5 TWSAG960U1CP  NTSAG960U1CP TFSAS367VC3PWP  STSAG965U1GP5 STSAG960U1CP (Hotspot SMKOK779HSDGWHP5 SM128PSIMT5RM  SMAPPMB20030  )  ( Carconnect SMKOK6500TGP5 TF128PSIMT5A
          dbms_output.put_line ('SIM='|| sa.get_test_sim('SM128PSIMT5N'));
          ---  SM128PSIMT5N TF256PSIMV97N  TF128SIMC4N TF128PSIMT5N
          dbms_output.put_line ('PIN='|| sa.get_test_pin('SMNAPP0050BBUNL'));
          -- SMNAPP0025TT  SMNAPP40030ILD  SMNAPP0050BBUNL  SMNAPP30060  TFAPP1SPHU0020 TWN00060 TS40120 STAPP20055 NTN6U060 TFAPP1SPHU0020 TWN00035 TWN00085 TWAPP00110      TWAPP00060
          End;
		 */
		type(SimplyMobile_androidObjects.simCardField, "8901260321259600231");
		ExtentTestManager.getTest().log(Status.PASS, "Entered the Sim Card Number");
	}

	//user select the checkbox for terms and conditions
	public void checkTermsAndConditions() {
		click(SimplyMobile_androidObjects.checkBox);
		ExtentTestManager.getTest().log(Status.PASS, "Check box selected for terms and conditions");
		
	}
	//user clicks on continue button in DeviceType page
	public void sMPContinueButton() {
		click(SimplyMobile_androidObjects.simCardContinue);
		ExtentTestManager.getTest().log(Status.PASS, "Clicked on continue button");
	}
	
	public void selectDeviceTypeAndContinue() {
		Wait(30);
		selectDeviceType();
		Wait(5);
		enterSimCardNumber();
		checkTermsAndConditions();
		sMPContinueButton();
	}

}
