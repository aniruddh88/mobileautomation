package androidPages;

import java.io.IOException;

import com.Tracfone.driverInstance.DTAFCommandBase_Android;
import com.Tracfone.extent.ExtentReporter;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.SimplyMobile_androidObjects;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class Notifications extends com.Tracfone.driverInstance.DTAFCommandBase_Android {

	ExtentReporter _extentReport = new ExtentReporter();

	public Notifications(AndroidDriver<MobileElement> androidDriver, String Platform)
			throws IOException, InterruptedException {
		super(androidDriver, Platform);
		this.androidDriver = androidDriver;
		System.out.println("Driver in Base Class Android is" + androidDriver);
	}

	/*******************************************************
	 * Accepting the Notifications
	 * 
	 * @param Endpoint URL
	 * 
	 *******************************************************/

	public void notifications_accept() {
		System.out.println("In Notification Accept Method");
		//Wait(30);
		checkElementPresent(SimplyMobile_androidObjects.welcome_Text);
		 String welcomeText = findElement(SimplyMobile_androidObjects.welcome_Text).getText();
		 System.out.println("Welcome Page Text--->>>"+welcomeText);
		 welcomeText.contains("Welcome to My Account!");
		 ExtentTestManager.getTest().log(Status.INFO,"Validating the Welcome Screen expected to contain found" + welcomeText);
		 
		click(SimplyMobile_androidObjects.remindLater);
		click(SimplyMobile_androidObjects.continueButton);
		click(SimplyMobile_androidObjects.phonePermission);
		click(SimplyMobile_androidObjects.continueButton);
		click(SimplyMobile_androidObjects.phonePermission);
		click(SimplyMobile_androidObjects.locationPermission);
		click(SimplyMobile_androidObjects.phonePermissionDeny);
		Wait(5);
		clearTextField(SimplyMobile_androidObjects.endPointText);
		type(SimplyMobile_androidObjects.endPointText, "sit1apifull.tracfone.com");
		click(SimplyMobile_androidObjects.acceptButton);
		ExtentTestManager.getTest().log(Status.PASS, "Enabled all the Notifications.!");
	}
	
	public void clickRemindLater() {
		click(SimplyMobile_androidObjects.remindLater);
	}
	
	public void clickDismiss() {
		Wait(15);
		click(SimplyMobile_androidObjects.dismissButton);
	}

}