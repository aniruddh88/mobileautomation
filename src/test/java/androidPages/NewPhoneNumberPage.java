package androidPages;

import java.io.IOException;

import com.Tracfone.driverInstance.DTAFCommandBase_Android;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.SimplyMobile_androidObjects;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class NewPhoneNumberPage extends DTAFCommandBase_Android{

	public NewPhoneNumberPage(AndroidDriver<MobileElement> androidDriver, String Platform)
			throws IOException, InterruptedException {
		super(androidDriver, Platform);
		this.androidDriver = androidDriver;
		System.out.println("Driver in Base Class Android is" +androidDriver);
	}
	
	//user enters the zip code
	public void enterZipCode() {
		click(SimplyMobile_androidObjects.zipCodeField);
		Wait(5);
		type(SimplyMobile_androidObjects.zipCodeField, "32718");
		ExtentTestManager.getTest().log(Status.PASS, "Entered zip code value");
		
	}

	//user clicks on continue button after enters zip code
	public void zipCodeContinue() {
		click(SimplyMobile_androidObjects.zipCodeContinue);
		ExtentTestManager.getTest().log(Status.PASS, "Clicked on continue button");
	}
	
	public void enterZipCodeAndContinue() {
		checkElementPresent(SimplyMobile_androidObjects.newPhoneNumberTitle);
		 String newPhoneNumberPageTitle = findElement(SimplyMobile_androidObjects.newPhoneNumberTitle).getText();
		 System.out.println("New Phone Number Page Text--->>>"+newPhoneNumberPageTitle);
		 newPhoneNumberPageTitle.contains("New Phone Number");
		 ExtentTestManager.getTest().log(Status.INFO,"Validating the New Phone Number Screen expected to contain found" + newPhoneNumberPageTitle);
		enterZipCode();
		zipCodeContinue();
	}

}
