package androidPages;

import java.io.IOException;

import com.Tracfone.driverInstance.DTAFCommandBase_Android;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.SimplyMobile_androidObjects;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class ServicePlanPage  extends DTAFCommandBase_Android{

	public ServicePlanPage(AndroidDriver<MobileElement> androidDriver, String Platform)
			throws IOException, InterruptedException {
		super(androidDriver, Platform);
		this.androidDriver = androidDriver;
		System.out.println("Driver in Base Class Android is" +androidDriver);
	}
	
	//user enters clicks on  servicePlanPin
	public void clickServicePlanWithPin() {		
		click(SimplyMobile_androidObjects.servicePlanWithPin);
		ExtentTestManager.getTest().log(Status.PASS, "Select the option service plan with pin");
	}

	//user enters servicePin
	public void enterServicePin() {
		checkElementPresent(SimplyMobile_androidObjects.enterPinTitle);
		 String enterPinPageTitle = findElement(SimplyMobile_androidObjects.enterPinTitle).getText();
		 System.out.println("Enter Pin Page Text--->>>"+enterPinPageTitle);
		 enterPinPageTitle.contains("Enter SIM Card Number");
		 ExtentTestManager.getTest().log(Status.INFO,"Validating the Enter Pin Screen expected to contain found" + enterPinPageTitle);
		click(SimplyMobile_androidObjects.servicePinField);
		Wait(5);
		//get the servicePin by running the below query 
				/*set serveroutput on
				  Begin
				  dbms_output.put_line ('ESN :'|| sa.get_test_esn('SMSAG965U1GP5'));
				  --Enter part number SMSAG965U1GP5 TWSAG960U1CP  NTSAG960U1CP TFSAS367VC3PWP  STSAG965U1GP5 STSAG960U1CP (Hotspot SMKOK779HSDGWHP5 SM128PSIMT5RM  SMAPPMB20030  )  ( Carconnect SMKOK6500TGP5 TF128PSIMT5A
		          dbms_output.put_line ('SIM='|| sa.get_test_sim('SM128PSIMT5N'));
		          ---  SM128PSIMT5N TF256PSIMV97N  TF128SIMC4N TF128PSIMT5N
		          dbms_output.put_line ('PIN='|| sa.get_test_pin('SMNAPP0050BBUNL'));
		          -- SMNAPP0025TT  SMNAPP40030ILD  SMNAPP0050BBUNL  SMNAPP30060  TFAPP1SPHU0020 TWN00060 TS40120 STAPP20055 NTN6U060 TFAPP1SPHU0020 TWN00035 TWN00085 TWAPP00110      TWAPP00060
		          End;
				 */
		type(SimplyMobile_androidObjects.servicePinField, "999999938632333");
		ExtentTestManager.getTest().log(Status.PASS, "Entered service pin number");
		
	}

	//user enters on continue buttons after enters servicePin
	public void servicePinContinue(){
		click(SimplyMobile_androidObjects.servicePinContinue);
		ExtentTestManager.getTest().log(Status.PASS, "Clicked on continue button");
	}

	//user clicks on Create Account option
	public void clickCreateAccount(){
		Wait(30);
		click(SimplyMobile_androidObjects.createAccount);
		ExtentTestManager.getTest().log(Status.PASS, "Clicked on create account option");
	}

	public void enterServicePlanPinAndCreate() {
		Wait(30);
		checkElementPresent(SimplyMobile_androidObjects.servicePlanTitle);
		 String servicePlanPageTitle = findElement(SimplyMobile_androidObjects.servicePlanTitle).getText();
		 System.out.println("Service Plan Page Text--->>>"+servicePlanPageTitle);
		 servicePlanPageTitle.contains("Service Plan");
		 ExtentTestManager.getTest().log(Status.INFO,"Validating the Service Plan Screen expected to contain found" + servicePlanPageTitle);
		clickServicePlanWithPin();
		Wait(5);
		enterServicePin();
		servicePinContinue();
		Wait(5);
		clickCreateAccount();
	}
	
}
