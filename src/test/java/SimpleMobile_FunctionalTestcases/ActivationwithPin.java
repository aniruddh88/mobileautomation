package SimpleMobile_FunctionalTestcases;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.apache.commons.exec.ExecuteException;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.Tracfone.driverInstance.DTAFCommandBase_Android;
import com.Tracfone.driverInstance.DTAFDriverInstance;
import com.Tracfone.extent.ExtentReporter;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.SimplyMobile_androidObjects;
import com.Tracfone.propertyfilereader.PropertyFileReader;
import com.aventstack.extentreports.Status;

import androidPages.ActivationTypePage;
import androidPages.CreateAccountPage;
import androidPages.DeviceConfigurationPage;
import androidPages.DeviceTypePage;
import androidPages.HomePage;
import androidPages.LoginPage;
import androidPages.MyAccountSummaryPage;
import androidPages.NewPhoneNumberPage;
import androidPages.Notifications;
import androidPages.ServicePlanPage;
import androidPages.TransactionSummaryPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import jxl.read.biff.BiffException;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

@Listeners(ExtentReporter.class)
public class ActivationwithPin  {
	

	
	public ExtentReporter _extentReport;
	
	DesiredCapabilities caps = new DesiredCapabilities();
	public  DTAFCommandBase_Android _caller;
	public LoginPage _loginPage;
	public HomePage _homePage;
	public Notifications _notify;
	public AndroidDriver<MobileElement> androidDriver;
	public DeviceTypePage _deviceTypePage;
	public ActivationTypePage _activationType;
	public NewPhoneNumberPage _newPhoneNumberPage;
	public ServicePlanPage _servicePlanPage;
	public CreateAccountPage _createAccountPage;
	
	public DeviceConfigurationPage _deviceConfigurationPage;
	public TransactionSummaryPage _transactionSummaryPage;
	public MyAccountSummaryPage _myAccountSummaryPage;
	
	
	@BeforeClass
	public void startAppiumServer() throws IOException {
		try {
			System.out.println("Starts here..!!!");
							
			// PCloudy Driver details
			androidDriver = new AndroidDriver<MobileElement>(new URL("https://poc.pcloudy.com/appiumcloud/wd/hub"),DTAFDriverInstance.generateAndroidCapabilities("Tracfone"));
			
			_caller = new DTAFCommandBase_Android(androidDriver,"Android");
		} catch (InterruptedException e) {

			System.out.println("capabilities Set");
			e.printStackTrace();
		}

	}

	
	@Test(description = "Activate a device and Create Account", priority =1)
	public synchronized void TC_ActivateDeviceCreateAccount()throws ExecuteException, IOException, InterruptedException, BiffException {
		try {
		 _extentReport = new ExtentReporter();
			
		System.out.println("###############################################");
		System.out.println("Execution Started");
		ExtentTestManager.getTest().log(Status.INFO, "Device Name : SAMSUNG_GalaxyS8_Android_9.0.0_9ed43");
		ExtentTestManager.getTest().log(Status.INFO, "Testcase Name : ActivateDeviceCreateAccount");
		System.out.println("###############################################");
		
		 _caller.Wait(40);
	
		 _notify = new Notifications(androidDriver,"Android");
		  _notify.notifications_accept();
		 
		//HomePage - add a new device 
		  _homePage = new HomePage(androidDriver,"Android");
		  _homePage.clickOnActivateDevice();
		  
		//DeviceType - select deviceType and clicks continue
		  _deviceTypePage = new DeviceTypePage(androidDriver,"Android");
		  _deviceTypePage.selectDeviceTypeAndContinue();
		  
		//ActivationType - click on new phoneNumber option
		  _activationType = new ActivationTypePage(androidDriver,"Android");
		  _activationType.clickNewPhoneNumber();
		  
		//NewPhoneNumberPage - click on new phoneNumber option
		  _newPhoneNumberPage = new NewPhoneNumberPage(androidDriver,"Android");
		  _newPhoneNumberPage.enterZipCodeAndContinue();
		  
		//ServicePlanPage - click on new phoneNumber option
		  _servicePlanPage = new ServicePlanPage(androidDriver,"Android");
		  _servicePlanPage.enterServicePlanPinAndCreate();
		  
		//CreateAccount - enter the data for create an account
		  _createAccountPage = new CreateAccountPage(androidDriver,"Android");
		  _createAccountPage.enterDataForAccountCreation();
		
			/*
			 * //DeviceConfigurationPage - enter the data for create an account
			 * _deviceConfigurationPage = new
			 * DeviceConfigurationPage(androidDriver,"Android");
			 * _deviceConfigurationPage.continueDC();
			 * 
			 * //DeviceConfigurationPage - enter the data for create an account
			 * _transactionSummaryPage = new
			 * TransactionSummaryPage(androidDriver,"Android");
			 * _transactionSummaryPage.doneButton();
			 * 
			 * _notify.clickDismiss();
			 */
			/*
			 * //MyAccountSummaryPage - verify title of the page _myAccountSummaryPage = new
			 * MyAccountSummaryPage(androidDriver,"Android");
			 * _myAccountSummaryPage.verifyMyAccountSummaryPageTitle();
			 */
		  
		   ExtentTestManager.getTest().log(Status.PASS, "Completed the Execution for Android");
	}
	catch(Exception e) {
		
		
		}
	}
	
	@AfterMethod
	public void stopAppiumServer(ITestResult result) throws ExecuteException, IOException, InterruptedException {
		if (result.getStatus() == ITestResult.FAILURE)
			
			//_extentReport.screencapture_iOS(result.getTestName(),iosDrive);
		
		_caller.tearDown();
	}

}
