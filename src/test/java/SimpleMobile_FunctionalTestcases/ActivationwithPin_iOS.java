package SimpleMobile_FunctionalTestcases;
import java.io.IOException;
import java.net.URL;
import org.apache.commons.exec.ExecuteException;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.Tracfone.driverInstance.DTAFCommandBase_iOS;
import com.Tracfone.driverInstance.DTAFDriverInstance;
import com.Tracfone.extent.ExtentReporter;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.propertyfilereader.PropertyFileReader;
import com.aventstack.extentreports.Status;
import iOSPages.Notifications;
import iOSPages.RegisterPage;
import iOSPages.ServicePlanPage;
import iOSPages.ActivationType;
import iOSPages.DeviceTypePage;
import iOSPages.DeviceTypes;
import iOSPages.EndPointPage;
import iOSPages.EnterPinPage;
import iOSPages.HomePage;
import iOSPages.LoginPage;
import iOSPages.NewPhoneNumberPage;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import jxl.read.biff.BiffException;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

@Listeners(ExtentReporter.class)
public class ActivationwithPin_iOS  
{
	
	public ExtentReporter _extentReport;
	DesiredCapabilities caps = new DesiredCapabilities();
	public  DTAFCommandBase_iOS _caller;
	public LoginPage _loginPage;
	public HomePage _homePage;
	public Notifications _notify;
	public EndPointPage _endPoint;
	public DeviceTypePage _deviceType;
	public DeviceTypes _deviceSelection;
	public ActivationType _activate;
	public NewPhoneNumberPage _newphone;
	public ServicePlanPage _serviceplan;
	public EnterPinPage _enterpin;
	public RegisterPage _register;
	public IOSDriver<MobileElement> iosDrive;
	
		
	@BeforeClass
	public void startAppiumServer() throws IOException {
		try {
			System.out.println("Starts here..!!!");
							
			// PCloudy Driver details
			 iosDrive = new IOSDriver<MobileElement>(new URL("https://poc.pcloudy.com/appiumcloud/wd/hub"),DTAFDriverInstance.generateiOSCapabilities("Tracfone"));
			_caller = new DTAFCommandBase_iOS(iosDrive, "iOS");
		} 
		catch (InterruptedException e) {

			System.out.println("capabilities Set");
			e.printStackTrace();
		}

	}

	@Test(description = "Activation with Pin", priority =1)
	public synchronized void TC_ActivationwithPin()throws ExecuteException, IOException, InterruptedException, BiffException {
		try {
		 _extentReport = new ExtentReporter();
		PropertyFileReader handler = new PropertyFileReader("properties/UserCredentials.properties");
		System.out.println("###############################################");
		System.out.println("Execution Started");
		ExtentTestManager.getTest().log(Status.INFO, "Device Name : Iphone 12; OS : 14.3");
		ExtentTestManager.getTest().log(Status.INFO, "Testcase Name : Activation with Pin Flow");
		System.out.println("###############################################");
		
		 	  _caller.Wait(50);
		 	  
			   // Notification Enablement
		      _notify = new Notifications(iosDrive,"iOS");
		      _notify.notifications_accept();
		  
		       // End Point URL Selection
		      _homePage = new HomePage(iosDrive,"iOS");
		      _homePage.endpointSelection();
		      
		       // End Point Updation
		      _endPoint = new EndPointPage(iosDrive,"iOS");
		      _endPoint.endpoint();
		  
		      //Home Page Menu Button selection
		      _homePage.menuButtonClick();
		      _homePage.activateDeviceClick();
		 
		      // Device Type Selection
		      _deviceSelection = new DeviceTypes(iosDrive,"iOS");
		      _deviceSelection.deviceTypeSelection();
		 
		      // Device Type Page
		      _deviceType = new DeviceTypePage(iosDrive,"iOS");
		      _deviceType.deviceTypeScreenValidation();
		      _deviceType.simCardDetails();
		  
		      // Activation Type Page
		      _activate = new ActivationType(iosDrive,"iOS");
		      _activate.ActivationTypeScreenValidation();
		      _activate.clickNewPhoneNumber();
		 
		      // 
		  	  _newphone = new NewPhoneNumberPage(iosDrive,"iOS");
		  	  _newphone.NewPhoneNumberScreenValidation();
			  _newphone.enterZipCode();
			  
			  _serviceplan = new ServicePlanPage(iosDrive,"iOS");
			  _serviceplan.ServicePlanScreenValidation();
			  _serviceplan.servicePlanClick();
			  
			  _enterpin = new EnterPinPage(iosDrive,"iOS");
			  _enterpin.EnterPinScreenValidation();
			  _enterpin.enterPin();
			  _enterpin.clickCreateButton();
			  
			  _register = new RegisterPage(iosDrive,"iOS");
			  _register.ResgisterPageScreenValidation();
			  _register.registerPageFill();
					  
		   ExtentTestManager.getTest().log(Status.PASS, "Completed the Activation with PIN flow in iOS Successfully.!");
	}
	catch(Exception e) {
		
		
		}
	}
	
	
	
	@AfterMethod
	public void stopAppiumServer(ITestResult result) throws ExecuteException, IOException, InterruptedException {
		if (result.getStatus() == ITestResult.FAILURE)
			
			//_extentReport.screencapture_iOS(result.getTestName(),iosDrive);
		
		_caller.tearDown();
	}

}
