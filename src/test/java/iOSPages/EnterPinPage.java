package iOSPages;

import java.io.IOException;

import com.Tracfone.driverInstance.DTAFCommandBase_iOS;
import com.Tracfone.extent.ExtentReporter;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.Objects_iOS;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;


public class EnterPinPage extends DTAFCommandBase_iOS{

	
	ExtentReporter _extentReport = new ExtentReporter();
public EnterPinPage(IOSDriver<MobileElement> iosDrive,String Platform) throws IOException, InterruptedException {
		
		super(iosDrive,"iOS");
		this.iosDrive = iosDrive;
		System.out.println("Control in command base" +iosDrive);
}

	
  public void EnterPinScreenValidation()
 {
	  Wait(20);
	  checkElementPresent(Objects_iOS.enterPinLabel);
	 // checkElementPresent(SimplyMobile_iOSObjects.keepphoneNumber);
	  ExtentTestManager.getTest().log(Status.PASS, "Enter Pin Screen Page has been Loaded successfully");
	  	}
  
  public void enterPin() {

	  // Query
	  
	  
	  
	    type(Objects_iOS.pinText,"999999938714515");
	   // hideKeyboard();
	    ExtentTestManager.getTest().log(Status.PASS, "Pin has been entered");
	    click(Objects_iOS.enterPinLabel);
	    Wait(10);
	    click(Objects_iOS.continueButton);
  
  }
  
  public void clickCreateButton()
  
  {
	  
	  Wait(20);
	  click(Objects_iOS.createButton);
	  ExtentTestManager.getTest().log(Status.PASS, "Clicked on Create Button");
  }
  
  

}