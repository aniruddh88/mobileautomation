package iOSPages;

import java.io.IOException;

import com.Tracfone.driverInstance.DTAFCommandBase_iOS;
import com.Tracfone.extent.ExtentReporter;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.Objects_iOS;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;


public class HomePage extends DTAFCommandBase_iOS{

	
	ExtentReporter _extentReport = new ExtentReporter();
public HomePage(IOSDriver<MobileElement> iosDrive,String Platform) throws IOException, InterruptedException {
		
		super(iosDrive,"iOS");
		this.iosDrive = iosDrive;
		System.out.println("Control in command base" +iosDrive);
}

/*******************************************************
 * 
 * Home Page Validation
 * 
 *******************************************************/	
public void homescreenValidation()
{
	
Wait(30); 
	
}

public void endpointSelection()
{
	click(Objects_iOS.endpointLink);
	ExtentTestManager.getTest().log(Status.PASS, "EndPoint Link is Clicked");
	}

public void menuButtonClick()

{
	click(Objects_iOS.menuButton);
	ExtentTestManager.getTest().log(Status.PASS, "Menu Button is Clicked");
	}

public void activateDeviceClick()
{
	click(Objects_iOS.activateDevice);
	ExtentTestManager.getTest().log(Status.PASS, "Clicked on Activate the Device");
}

public void simplemobilephoneClick() {

	click(Objects_iOS.simpleMobilePhone);
	ExtentTestManager.getTest().log(Status.PASS, "Clicked on Simple mobile phone.!");
}

		
}