package iOSPages;

import java.io.IOException;

import com.Tracfone.driverInstance.DTAFCommandBase_iOS;
import com.Tracfone.extent.ExtentReporter;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.Objects_iOS;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;


public class EndPointPage extends DTAFCommandBase_iOS{

	
	ExtentReporter _extentReport = new ExtentReporter();
public EndPointPage(IOSDriver<MobileElement> iosDrive,String Platform) throws IOException, InterruptedException {
		
		super(iosDrive,"iOS");
		this.iosDrive = iosDrive;
		System.out.println("Control in command base" +iosDrive);
}

/*******************************************************
 * 
 * This method is to specify the endpoint
 * @param endPoint URL
 * 
 *******************************************************/

public void endpoint()
{
	// Data to be fetched from property file
	
	type(Objects_iOS.endpointTextBox,"https://sit1apifull.tracfone.com/");
	click(Objects_iOS.applyButton);
	Wait(20);
	ExtentTestManager.getTest().log(Status.PASS, "EndPoint has been Applied");
	click(Objects_iOS.backButton);
	}
		
}