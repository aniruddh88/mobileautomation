package iOSPages;

import java.io.IOException;

import com.Database_ReusableMethods.Queries;
import com.Database_ReusableMethods.SQLDatabaseConnection;
import com.Tracfone.driverInstance.CSVReader;
import com.Tracfone.driverInstance.DTAFCommandBase_iOS;
import com.Tracfone.extent.ExtentReporter;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.Objects_iOS;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;


public class DeviceTypePage extends DTAFCommandBase_iOS{

	
	ExtentReporter _extentReport = new ExtentReporter();
public DeviceTypePage(IOSDriver<MobileElement> iosDrive,String Platform) throws IOException, InterruptedException {
		
		super(iosDrive,"iOS");
		this.iosDrive = iosDrive;
		System.out.println("Control in command base" +iosDrive);
}
/*******************************************************
 * 
 * This method is to validate Device Type page
 * 
 *******************************************************/
	
  public void deviceTypeScreenValidation()
 {
	  Wait(20);
	  checkElementPresent(Objects_iOS.deviceTypeLabel);
	  checkElementPresent(Objects_iOS.simcardMessage);
	  ExtentTestManager.getTest().log(Status.PASS, "Device Type Screen has been loaded successfully");
	}
  
  /*******************************************************
   * 
   * This method is to provide the Sim card Details
 * @throws Exception 
   * 
   *******************************************************/
  
  public void simCardDetails() throws Exception {

		/*
		 * String csvFile = "C:\\Users\\suganyak\\Desktop\\SimpleMobile_activation.csv";
		 * 
		 * CSVReader.read(csvFile); System.out.println(csvFile);
		 */
		/*
		 * String[] data = csvFile.split("\n"); String EsnSimPin = data[1];
		 * System.out.println("The ESN, PIN & SIM number is.!"+EsnSimPin);
		 */
	  
	  
	  //SQLDatabaseConnection.DBConnection();
	  //SQLDatabaseConnection.DBConnection();
	 
	  //System.out.println(esnsimpin);
	  
	  type(Objects_iOS.simcardnumber,"8901260321162140593");
	  Wait(10);
	  click(Objects_iOS.termscondition);
	  Wait(10);
	  click(Objects_iOS.continueButton);
	  ExtentTestManager.getTest().log(Status.PASS, "Entered the SIM card details.!");
  
  }
  
  

}