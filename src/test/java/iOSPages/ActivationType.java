package iOSPages;

import java.io.IOException;

import com.Tracfone.driverInstance.DTAFCommandBase_iOS;
import com.Tracfone.extent.ExtentReporter;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.Objects_iOS;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;


public class ActivationType extends DTAFCommandBase_iOS{

	
	ExtentReporter _extentReport = new ExtentReporter();
public ActivationType(IOSDriver<MobileElement> iosDrive,String Platform) throws IOException, InterruptedException {
		
		super(iosDrive,"iOS");
		this.iosDrive = iosDrive;
		System.out.println("Control in command base" +iosDrive);
}

/*******************************************************
 * 
 * This Method is to validate Activation Type Screen
 * 
 *******************************************************/
	
  public void ActivationTypeScreenValidation()
 {
	  Wait(20);
	  
	  checkElementPresent(Objects_iOS.activationTypeLabel);
	  checkElementPresent(Objects_iOS.keepphoneNumber);
	  ExtentTestManager.getTest().log(Status.PASS, "Activation Type Page has been loaded successfully.!");
	  	}
  /*******************************************************
   * 
   * This Method is to click on new phone number
   * 
   *******************************************************/
  
  public void clickNewPhoneNumber() {

	  
	  click(Objects_iOS.newPhoneNumber);
	  ExtentTestManager.getTest().log(Status.PASS, "Clicked on new phone number option");
  
  }
  
  

}