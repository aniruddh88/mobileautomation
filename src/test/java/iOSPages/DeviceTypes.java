package iOSPages;

import java.io.IOException;

import com.Tracfone.driverInstance.DTAFCommandBase_iOS;
import com.Tracfone.extent.ExtentReporter;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.Objects_iOS;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;


public class DeviceTypes extends DTAFCommandBase_iOS{

	
	ExtentReporter _extentReport = new ExtentReporter();
public DeviceTypes(IOSDriver<MobileElement> iosDrive,String Platform) throws IOException, InterruptedException {
		
		super(iosDrive,"iOS");
		this.iosDrive = iosDrive;
		System.out.println("Control in command base" +iosDrive);
}

/*******************************************************
 * 
 * This method is to select activate with own device 
 * option 
 * 
 *******************************************************/
  
  public void deviceTypeSelection() {

	   click(Objects_iOS.ownDevice);
	   ExtentTestManager.getTest().log(Status.PASS, "Clicked on activate your own device.!");
  
  }
  
  

}