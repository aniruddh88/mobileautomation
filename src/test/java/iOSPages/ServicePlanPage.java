package iOSPages;

import java.io.IOException;

import com.Tracfone.driverInstance.DTAFCommandBase_iOS;
import com.Tracfone.extent.ExtentReporter;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.Objects_iOS;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;


public class ServicePlanPage extends DTAFCommandBase_iOS{

	
	ExtentReporter _extentReport = new ExtentReporter();
public ServicePlanPage(IOSDriver<MobileElement> iosDrive,String Platform) throws IOException, InterruptedException {
		
		super(iosDrive,"iOS");
		this.iosDrive = iosDrive;
		System.out.println("Control in command base" +iosDrive);
}

/*******************************************************
 * 
 * This method is to validate Service plan screen
 * 
 *******************************************************/
  public void ServicePlanScreenValidation()
 {
	  Wait(20);
	  checkElementPresent(Objects_iOS.servicePlanLabel);
	 // checkElementPresent(SimplyMobile_iOSObjects.keepphoneNumber);
	  ExtentTestManager.getTest().log(Status.PASS, "Service Plan screen has been loaded successfully.!");
	  	}
  /*******************************************************
   * 
   * This method is to click on service plan purchase 
   * with pin button
   * 
   *******************************************************/
  
  public void servicePlanClick() {

	    click(Objects_iOS.purchasewithPin);
	    ExtentTestManager.getTest().log(Status.PASS, "Clicked on Purchase with Pin Button");
  
  }
  
  

}