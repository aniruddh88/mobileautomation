package iOSPages;

import java.io.IOException;

import com.Tracfone.driverInstance.DTAFCommandBase_iOS;
import com.Tracfone.extent.ExtentReporter;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.Objects_iOS;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;


public class NewPhoneNumberPage extends DTAFCommandBase_iOS{

	
	ExtentReporter _extentReport = new ExtentReporter();
public NewPhoneNumberPage(IOSDriver<MobileElement> iosDrive,String Platform) throws IOException, InterruptedException {
		
		super(iosDrive,"iOS");
		this.iosDrive = iosDrive;
		System.out.println("Control in command base" +iosDrive);
}

	
  public void NewPhoneNumberScreenValidation()
 {
	  Wait(20);
	  checkElementPresent(Objects_iOS.newPhonenumberLabel);
	 // checkElementPresent(SimplyMobile_iOSObjects.keepphoneNumber);
	  ExtentTestManager.getTest().log(Status.PASS, "New Phone number page has been loaded successfully");
	  	}
  
  public void enterZipCode() {

	  type(Objects_iOS.zipcode,"33132");
	  ExtentTestManager.getTest().log(Status.PASS, "Zip Code has been entered successfully");
	  click(Objects_iOS.continueNewPhone);
  
  }
  
  

}