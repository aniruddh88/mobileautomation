package iOSPages;

import java.io.IOException;
import com.Tracfone.extent.ExtentReporter;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.Objects_iOS;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;


public class Notifications extends com.Tracfone.driverInstance.DTAFCommandBase_iOS{

	
	ExtentReporter _extentReport = new ExtentReporter();
	public Notifications(IOSDriver<MobileElement> iosDrive, String Platform)throws IOException, InterruptedException 
	{
		super(iosDrive, Platform);
		this.iosDrive = iosDrive;
		System.out.println("Driver in Base Class IOS is" +iosDrive);
	}
	
	/*******************************************************
	 * 
	 * Accepting the Notifications
	 * 
	 *******************************************************/
	
	public void notifications_accept()
	{
		System.out.println("In Notification Accept Method");
		//Wait(30); 
		click(Objects_iOS.allowButton);
		Wait(10);
		click(Objects_iOS.agreeButton);
		Wait(20);
			
		ExtentTestManager.getTest().log(Status.PASS, "Enabled all the Notifications.!");
	}
	
}