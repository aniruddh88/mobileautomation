package iOSPages;

import java.io.IOException;

import com.Tracfone.driverInstance.DTAFCommandBase_iOS;
import com.Tracfone.extent.ExtentReporter;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.Objects_iOS;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;


public class RegisterPage extends DTAFCommandBase_iOS{

	
	ExtentReporter _extentReport = new ExtentReporter();
public RegisterPage(IOSDriver<MobileElement> iosDrive,String Platform) throws IOException, InterruptedException {
		
		super(iosDrive,"iOS");
		this.iosDrive = iosDrive;
		System.out.println("Control in command base" +iosDrive);
}

/*******************************************************
 * 
 * Registration Page Validation
 * 
 *******************************************************/
	
  public void ResgisterPageScreenValidation()
 {
	  Wait(20);
	  checkElementPresent(Objects_iOS.registerPageLabel);
	 // checkElementPresent(SimplyMobile_iOSObjects.keepphoneNumber);
	  	}
 
  /*******************************************************
   * New Account creation 
   * @param email ID,Password,Security Pin & Date of Birth
   * 
   *******************************************************/
  
  public void registerPageFill() {

	  
	  // Data to be fetch from Property File
	  
	   type(Objects_iOS.emailId,"260321259296283@sit1.com");
	   type(Objects_iOS.confirmemailId,"260321259296283@sit1.com");
	   type(Objects_iOS.password,"123456");
	   type(Objects_iOS.confirmPassword,"123456");
	   type(Objects_iOS.securityPin,"1234");
	   type(Objects_iOS.confirmSecurityPin,"1234");
	   type(Objects_iOS.DOBM,"08");
	   type(Objects_iOS.DOBD,"02");
	   type(Objects_iOS.DOBY,"2000");
	   ExtentTestManager.getTest().log(Status.PASS, "New Account has been created");
	   Wait(5);
	   //click(Objects_iOS.saveAccount);
	   
	   
	   
  
  }
  
  

}