package com.Tracfone.driverInstance;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import com.Tracfone.driverInstance.DTAFDriverInstance;
import com.Tracfone.excel.UtilityExcel;
import com.Tracfone.extent.ExtentReporter;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.mobileobjects.*;
import com.Tracfone.propertyfilereader.PropertyFileReader;
import com.aventstack.extentreports.Status;
import com.google.common.collect.Ordering;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.touch.offset.PointOption;

/**
 *
 */


public class DTAFCommandBase_iOS extends DTAFDriverInstance {

	/**
	 * Instantiates a new appium command base.
	 *
	 * @param platform
	 *            the device type
	 * @throws InterruptedException
	 * @throws IOException
	 */
	
	public IOSDriver<MobileElement> iosDrive;
	//ExtentReporter extentReport = new ExtentReporter(iosDrive,"iOS");	
	public DTAFCommandBase_iOS(IOSDriver<MobileElement> iosDrive,String Platform) throws IOException, InterruptedException {
		
		super(iosDrive,"iOS");
		this.iosDrive = iosDrive;
		System.out.println("Control in command base" +iosDrive);
		init();
	}

	ExtentReporter extent = new ExtentReporter();
	
	private SoftAssert softAssert = new SoftAssert();

	/** The Constant logger. */
	final static Logger logger = Logger.getLogger("rootLogger");

	/** The Android driver. */
	
	/** The time out. */
	private int timeOut;

	/**
	 * The retry count
	 */
	private int retryCount;

	/**
	 * @param retrycount
	 * @return the retrycount
	 */
	public int getRetryCount() {

		return retryCount;
	}

	/**
	 * @param retrycount
	 */
	public void setRetryCount(int retrycount) {
		this.retryCount = retrycount;
	}

	/**
	 * Gets the time out.
	 * 
	 * @return the time out
	 */
	public int getTimeOut() {
		return timeOut;
	}

	/**
	 * Sets the time out.
	 * 
	 * @param timeOut
	 *            the new time out
	 */
	public void setTimeOut(int timeOut) {
		this.timeOut = timeOut;
	}

	/**
	 * Initializes the execution parameters.
	 */
	protected void init() {
		System.out.println("Conrol in init commandbase" +iosDrive);
		
		
		PropertyFileReader handler = new PropertyFileReader("properties/Execution.properties");
		setTimeOut(Integer.parseInt(handler.getproperty("TIMEOUT")));
		setRetryCount(Integer.parseInt(handler.getproperty("RETRY_COUNT"))); 
		logger.info("Loaded the following properties" + " TimeOut :"+ getTimeOut() + " RetryCount :" + getRetryCount());
	}

	/**
	 * Find element in the web page.
	 * 
	 * @param byLocator
	 *            the by locator
	 * @return the web element
	 */
	public WebElement findElement(By byLocator) {
		WebElement element = (new WebDriverWait(iosDrive,getTimeOut())).until(ExpectedConditions.presenceOfElementLocated(byLocator));
		return element;
		}
	
		
	
	public List<MobileElement> findelements(By bylocator){

		List<MobileElement> element = null;
		try {
			element = iosDrive.findElements(bylocator);
			softAssert.assertTrue(!element.isEmpty());
			logger.info("List values stored in" + element );
		} catch (Exception e) {
			logger.error(e);
		}
		return element;
	}


	/**
	 * Wait for element in the web page.
	 * 
	 * @param byLocator
	 *            the by locator
	 */
	public void waitForElement(By byLocator) {

		findElement(byLocator);
		logger.info("Wait for web element " + byLocator + " to present.");
		ExtentTestManager.getTest().log(Status.INFO, "Waiting for the Web Element");
		//extent.extentLogger("waitForElement", "Waiting for the Web Element");

	}
	
	/*public void waitEnable(By byLocator) {

		try {
			WebElement element = findElement(byLocator);
			boolean isEnabled = element.isEnabled();			
			logger.info("Wait for web element to enable " + isEnabled + " to present.");
			extent.extentLogger("waitForElement", "Waiting for the Web Element to enable");

		} catch (Exception e) {
			logger.error(e);

		}
	}*/

	
	

	/**
	 * Type on a web element.
	 * 
	 * @param byLocator
	 *            the by locator
	 * @param text
	 *            the text
	 */
	public void type(By byLocator, String text) {

		try {
			
			WebElement element = findElement(byLocator);
			element.sendKeys(text);
			/*logger.info("Typed the value " + text + " in to object " + byLocator);
			extent.extentLogger("type", "Typed the Web Element" +" "+ element.toString());*/
			
			
		} catch (Exception e) {
			logger.error(e);

		}
	}

	
	
	/**
	 * Click on a web element.
	 * 
	 * @param byLocator
	 *            the by locator
	 * @throws IOException 
	 * 
	 */
	public void click(By byLocator)  {

		try {
			
			WebElement element = findElement(byLocator);
			element.click();
	    	/*logger.info("Clicked on the object" + byLocator);
			extent.extentLogger("click", "Clicked the Web Element" +" "+ element.toString());*/
			}
			

		 catch (Exception e) {
			 ExtentTestManager.getTest().log(Status.INFO,  "The element " + byLocator + "is not found");
			 //extent.extentLogger("Click on" , "The element " + byLocator + "is not found");
			 Assert.fail("Element not found");
				logger.error(e);

        }
	}

	
	
	/**
	 * get specific property value of a web element and stores to string
	 * variable.
	 * 
	 * @param property
	 *            the property of the element.
	 * @param byLocator
	 *            the by locator
	 * @return value of the property.
	 */
	public String getElementPropertyToString(String property, By byLocator) {
		String propertyValue = null;
		try {
			WebElement element = findElement(byLocator);
			propertyValue = element.getAttribute(property);
			logger.info("Stored the property value of the object " + byLocator
					+ " property :" + property + "value : " + propertyValue);
			
			//extent.extentLogger("getElementPropertyToString","Stored the property value of the object " +" "+ element.toString());
			 ExtentTestManager.getTest().log(Status.INFO,  "Stored the property value of the object " +" "+ element.toString());
		} catch (Exception e) {
			logger.error(e);
		}
		return propertyValue;
	}

	
	/**
	 * get all the values in the screen and store it in the string
	 * 
	 * @param byLocator
	 * @return
	 */
	public List<String> getAllValues(By byLocator) {

		List<String> list = new ArrayList<String>();

		try {
			List<MobileElement> wbList = findelements(byLocator);
			for (WebElement webElement : wbList) {
				list.add(webElement.getText());
				System.out.println(webElement);
				logger.info(webElement.getText());
			}
		} catch (Exception e) {
			logger.error(e);
		}
		return list;
	}

	
	/**
	 * Check element present.
	 *
	 * @param byLocator
	 *            the by locator
	 * @return true, if successful
	 * @throws IOException 
	 */
	public boolean checkElementPresent(By byLocator)  {

		try {
			findElement(byLocator);
			softAssert.assertEquals(findElement(byLocator).isDisplayed(), true, "The element " + byLocator +" "+ "is displayed");
		//	logger.info("The element " + byLocator +" "+ "is displayed");
			//extent.extentLogger("checkElementPresent" , "The element " + byLocator + "is displayed");
			return true;
		} catch (Exception e) {
			softAssert.assertEquals(false, true, "Element" + byLocator +" "+ "is not visible");
			softAssert.assertAll();
			logger.error("Element" + byLocator +" "+ "is not visible");
			 ExtentTestManager.getTest().log(Status.INFO,  "The element " + byLocator + "is not displayed");
			//extent.extentLogger("checkElementPresent" , "The element " + byLocator + "is not displayed");
			//extent.screencapture("Android_Screenshot");
			return false;
		}
	}

	/**
	 * Check element present.
	 * kkk
	 * @param byLocator
	 *            the by locator
	 * @return true, if successful
	 */
	public boolean checkElementEnable(By byLocator) {

		try {
			findElement(byLocator);
			softAssert.assertEquals(findElement(byLocator).isEnabled(), true, "The element " + byLocator +" "+ "is displayed");
			logger.info("The element " + byLocator +" "+ "is displayed");
			//extent.extentLogger("checkElementPresent" , "The element " + byLocator + "is Enabled");
			return true;
		} catch (Exception e) {
			softAssert.assertEquals(false, true, "Element" + byLocator +" "+ "is not Enabled");
			softAssert.assertAll();
			logger.error("Element" + byLocator +" "+ "is not visible");
			ExtentTestManager.getTest().log(Status.INFO,  "The element " + byLocator + "is not Enabled");
			//extent.extentLogger("checkElementPresent" , "The element " + byLocator + "is not Enabled");
			return false;
		}
	}

	
	
	
	
	
	/**
	 * @param byLocator
	 * @return true or false
	 */
      public boolean checkcondition(By byLocator){
		boolean iselementPresent = false;
		try {
		iselementPresent = iosDrive.findElement(byLocator).isDisplayed();
		iselementPresent = true;
		} catch (Exception e) {
		iselementPresent = false;
		} 
		return iselementPresent;
    	/*boolean isElementPresent = false;  
    	try {
		List<WebElement> array = getDriver().findElements(byLocator);
		if(array.size()>0){
		isElementPresent = true;	
		}
		} catch (Exception e) {
		isElementPresent = false;	
		}
		return isElementPresent;*/
    	}

	/**
	 * Lock the screen.
	 *
	 * @param int seconds to lock
	 */
	public void lockScreen() {

		try {
			iosDrive.lockDevice();
			logger.info("Screen is locked");
			//extent.extentLogger("lockScreen","Screen is locked");
			ExtentTestManager.getTest().log(Status.INFO,  "Screen is locked");
		} catch (Exception e) {
			logger.error(e);
		}
	}

	
	/**
	 * Put the application to background
	 * 
	 * @param x
	 */
	public void putApptoBackground(Duration x) {

		try {

			iosDrive.runAppInBackground(x);
			logger.info("Application is running in background for " + x
					+ "seconds");
			
			//extent.extentLogger("putApptoBackground","Application is running in background for " + x + "seconds");

		} catch (Exception e) {
			logger.error(e);
		}

	}

	
	/**
	 * Closes the Keyboard
	 */
	public void hideKeyboard() {
       try {
    	   iosDrive.hideKeyboard();
            logger.info("Hiding keyboard was Successfull");
          //  ExtentTestManager.getTest().log(Status.INFO,  "Hiding keyboard was Successfull");
			//extent.extentLogger("hideKeyboard","Hiding keyboard was Successfull");
		} catch (Exception e) {
			logger.error(e);
		}

	}

	
	/**
	 * Wait .
	 *
	 * @param x seconds to lock
	 */
	public void Wait(int x) {

		try {
			iosDrive.manage().timeouts().implicitlyWait(x, TimeUnit.SECONDS);
			//logger.info("Wait for " + x + "seconds");
			//ExtentTestManager.getTest().log(Status.INFO, "Wait for " + x + "seconds");
			//extent.extentLogger("Wait","Wait for " + x + "seconds");
            } catch (Exception e) {
			logger.error(e);
		}
	}

	
	
	
	/**
	 * Back
	 */
	public void Back(int x) {

		try {

			for(int i = 0; i <x; i++){
				iosDrive.navigate().back();	
				logger.info("Back button is tapped");
				ExtentTestManager.getTest().log(Status.INFO,"Back button is tapped");
				//extent.extentLogger("Back","Back button is tapped");
			}
           } catch (Exception e) {
			logger.error(e);
		}
	}

	/**
	 * Kill application.
	 */
	public void KillApp() {

		try {
			iosDrive.closeApp();
			logger.info("Application is Killed");
			ExtentTestManager.getTest().log(Status.INFO,"Application is Killed");
			//extent.extentLogger("KillApp", "Application is Killed");
		} catch (Exception e) {
			logger.error(e);
		}
	}

	/**
	 * Relaunch the application.
	 */
	public void RelaunchApp() {

		try {
			iosDrive.launchApp();

			logger.info("Application is Re-launched");
			//extent.extentLogger("RelaunchApp","Application is Re-launched");
		} catch (Exception e) {
			logger.error(e);
		}
	}


	/**
	 * Rotate the screen in portrait or landscape mode.
	 *
	 * @param text
	 */
	public void rotateDevice(String orientation) {

		try {
			iosDrive.rotate(ScreenOrientation.valueOf(orientation));
			logger.info("Screen orinetation for " + orientation
					+ "mode was succesfull");
			//extent.extentLogger("rotateDevice","Screen orinetation for " + orientation + "mode was succesfull");
		} catch (Exception e) {
			logger.error(e);
		}
	}


	/**
	 * @param dire swipes the screen in the user specified direction (LEFT, RIGHT, UP, DOWN)
	 * 
	 * @param byLocator
	 */
	/*
	 * public void dragToElement(String dire, By byLocator) {
	 * 
	 * do { try { findElement(byLocator); break; } catch (Exception
	 * NoSuchElementException) {
	 * 
	 * if (dire.equalsIgnoreCase("LEFT")) {
	 * 
	 * Dimension size = iosDrive.manage().window().getSize(); int startx = (int)
	 * (size.width * 0.8); int endx = (int) (size.width * 0.20); int starty =
	 * size.height / 2; iosDrive.swipe(startx, starty, endx, starty, 1000);
	 * 
	 * } else if (dire.equalsIgnoreCase("RIGHT")) {
	 * 
	 * Dimension size = iosDrive.manage() .window().getSize(); int endx = (int)
	 * (size.width * 0.8); int startx = (int) (size.width * 0.20); int starty =
	 * size.height / 2; iosDrive.swipe(startx, starty, endx, starty, 1000);
	 * 
	 * } else if(dire.equalsIgnoreCase("UP")){
	 * 
	 * Dimension size = iosDrive.manage().window().getSize(); int starty = (int)
	 * (size.height * 0.80); int endy = (int) (size.height * 0.20); int startx =
	 * size.width / 2; iosDrive.swipe(startx, starty, startx, endy, 3000);
	 * 
	 * 
	 * } else if(dire.equalsIgnoreCase("DOWN")){
	 * 
	 * Dimension size = iosDrive.manage().window().getSize(); int starty = (int)
	 * (size.height * 0.80); int endy = (int) (size.height * 0.20); int startx =
	 * size.width / 2; iosDrive.swipe(startx, endy, startx, starty, 3000);
	 * 
	 * } } } while(true); }
	 */

	/**
	 * Swipes the screen in left or right or Up or Down or direction
	 * 
	 * @param direction
	 *            to swipe Left or Right or Up or Down
	 * @param count
	 *            to swipe
	 */


	public void Swipe(String direction, int count) {

		String dire = direction;

		try {


			if (dire.equalsIgnoreCase("LEFT")) {

				for (int i = 0; i < count; i++) {
					Dimension size = iosDrive.manage().window().getSize();
					int startx = (int) (size.width * 0.5);
					int endx = (int) (size.width * 0.20);
					int starty = size.height / 2;

					TouchAction ta = new TouchAction(iosDrive);
					ta.press(PointOption.point(startx, starty)).moveTo(PointOption.point(endx, starty)).release()
							.perform();
				}
			}  else if (dire.equalsIgnoreCase("RIGHT")) {

				for (int j = 0; j < count; j++) {
					Dimension size = iosDrive.manage().window().getSize();
					int endx = (int) (size.width * 0.8);
					int startx = (int) (size.width * 0.20);
					int starty = size.height / 2;

					TouchAction ta = new TouchAction(iosDrive);
					ta.press(PointOption.point(startx, starty)).moveTo(PointOption.point(endx, starty)).release()
							.perform();
				}
			} else if(dire.equalsIgnoreCase("UP")){

				for (int j = 0; j < count; j++) {
				  Dimension size = iosDrive.manage().window().getSize();
				  int starty = (int) (size.height * 0.80);
				  int endy = (int) (size.height * 0.20);
				  int startx = size.width / 2;
		       new TouchAction(iosDrive).longPress(PointOption.point(startx, starty)).moveTo(PointOption.point(startx,endy)).release().perform();
				logger.info("Swiping the screen in " +" "+ dire + "direction for" +" "+ count);
				//extent.extentLogger("SwipeRight","Swiping the screen in " +" "+ dire + "direction for" +" "+ count);
			
				}
			} else if (dire.equalsIgnoreCase("DOWN")) {
				
				for (int j = 0; j < count; j++) {
					Dimension size = iosDrive.manage().window().getSize();
					int starty = (int) (size.height * 0.80);
					int endy = (int) (size.height * 0.20);
					int startx = size.width / 2;
					TouchAction ta = new TouchAction(iosDrive);
					ta.press(PointOption.point(startx, endy)).moveTo(PointOption.point(startx, starty)).release()
							.perform();
				}
			}

		} catch (Exception e) {
			logger.error(e);

		}

	}

	/**
	 * Kill or start an application using activity
	 * 
	 * @param command
	 *           to START or KILL an application
	 * @param activity
	 *           Start an application by passing the activity 
	 */          
	public void adbStartKill(String command , String activity){
		String cmd;
		try {
			if(command.equalsIgnoreCase("START")){
				cmd = "adb shell am start -n" +" "+activity;
				Runtime.getRuntime().exec(cmd);
				logger.info("Started the activity" + cmd);
				//extent.extentLogger("adbStart","Started the activity" + cmd);
			}else if (command.equalsIgnoreCase("KILL")) {
				cmd = "adb shell am force-stop"+" "+activity;
				Runtime.getRuntime().exec(cmd);
				logger.info("Executed the App switch");
				//extent.extentLogger("adbKill","Executed the App switch");

			}   
		} catch (Exception e) {
			logger.error(e);
		}	   
	}


	/**
	 * @param keyevent
	 *        pass the android key event value to perform specific action
	 * 
	 */
	public void adbKeyevents(int keyevent){

		try {
			String cmd = "adb shell input keyevent" +" "+keyevent;
			Runtime.getRuntime().exec(cmd);
			logger.info("Performed the Keyvent" + keyevent);
			//extent.extentLogger("adbKeyevent","Performed the Keyvent" + keyevent);
		} catch (Exception e) {
			logger.error(e);
		}

	}


	/**
	 * @param byLocator
	 * @returns the list count of the element 
	 */
	public int getCount(By byLocator){

		int count = 0;
		try {		
			count = iosDrive.findElements(byLocator).size();
			logger.info("List count for" +" "+ byLocator +" "+ "is"+" "+ count);	
			//extent.extentLogger("getCount","List count for" +" "+ byLocator +" "+ "is"+" "+ count);
		} catch (Exception e) {
			logger.error(e);	
		}
		return count;
	}


	/**
	 * @param appid
	 * @return true if application is installed in the device
	 */
	public boolean checkIsAppInstalled(String appid) {

		try {
              if (iosDrive.isAppInstalled(appid)) {
				logger.info("Application is installed" + " " + appid);
				//extent.extentLogger("checkIsAppInstalled","Application is installed" + " " + appid);
				return true;
			}

		} catch (Exception e) {
			logger.info("Application is not installed");
		}
          return false;
   }


	/**
	 * @param apppath
	 */
	public void installapp(String apppath){

		try {

			iosDrive.installApp(System.getProperty("user.dir") + apppath);
            logger.info("Successfully installed the application into the device");
			//extent.extentLogger("installapp","Successfully installed the application into the device");
		} catch (Exception e) {
			logger.error("Application installed failed");
		}

	}


	/**
	 * @param apppath
	 *  Uninstalls the application
	 */
	public void uninstallapp(String apppath){

		try {
			iosDrive.removeApp(apppath);
			logger.info("Application removed successfully");
		//	extent.extentLogger("uninstallapp","Application removed successfully");
		} catch (Exception e) {
			logger.error("Application not removed");
		}

	}


	/**
	 * Gets the current activity
	 * 
	 */
	/*public void getCurrentActivity(){

		try {
			androidDriver = (AndroidDriver<MobileElement>)getDriver();
			androidDriver.currentActivity();
			logger.info("Current activity of the application is " + " "+ androidDriver.currentActivity());
			extent.extentLogger("getCurrentActivity","Current activity of the application is " + " "+ androidDriver.currentActivity());
		} catch (Exception e) {
			logger.error("There are no any current activity");
		}

	}*/


	/**
	 * @param appPackage
	 * @param appActivity 
	 * 
	 * Starts the given activity
	 */
/*	public void startActivity(String appPackage, String appActivity ){

		try {
			androidDriver = (AndroidDriver<MobileElement>)getDriver();
			androidDriver.startActivity(appPackage, appActivity, null, null);
			logger.info("Started the following activity");
			extent.extentLogger("startActivity","Started the following activity");
		} catch (Exception e) {
			logger.error("Unable to start the following activity");
		}
	}
	*/
	
	
	/**
	 * Finding the duplicate elements in the list
	 * @param mono
	 * @param content
	 * @param dosechang
	 * @param enteral
	 */
	public List<String> findDuplicateElements(List<String> mono){

		List<String> duplicate = new ArrayList<String>();
		Set<String> s = new HashSet<String>();
		try {
            if(mono.size()>0){
                  for(String content : mono){
                       if(s.add(content) == false){
						int i=1;
						duplicate.add(content);
						System.out.println("List of duplicate elements is" + i + content );
						i++;
					} 
				} 
			}
         } catch (Exception e) {
			System.out.println(e);
		}
		return duplicate;
	}

	
	/**
	 * @param contents
	 * @return the list without duplicate elements
	 */
	public List<String> removeDuplicateElements(List<String> contents){
		
		 LinkedHashSet<String> set = new LinkedHashSet<String>(contents);
		 ArrayList<String> listWithoutDuplicateElements = new ArrayList<String>();
		 try {
			 
			 if(contents.size()>0){
				 listWithoutDuplicateElements = new ArrayList<String>(set);
			}
			
		} catch (Exception e) {
			System.out.println(e);
		}
		return listWithoutDuplicateElements;
	}
	
	
	
	/**
	 * @param sorted
	 * @return true if the list is sorted
	 * @return false if the list is not sorted
	 */
	public boolean checkListIsSorted(List<String> ListToSort){
		
		boolean isSorted = false ;
		
		if(ListToSort.size()>0){
			try {
				if(Ordering.natural().isOrdered(ListToSort)){
					//extent.extentLogger("Check sorting", "List is sorted");
					logger.info("List is sorted");
					isSorted = true;
				    return isSorted;	
				}else{
					//extent.extentLogger("Check sorting", ListToSort +" "+"List is not sorted");
					logger.info(ListToSort + "List is notsorted");
					isSorted = false;
				}
			} catch (Exception e) {
			     System.out.println(e);
			}
		} else {
			logger.info("The size of the list is zero");
			//extent.extentLogger("", ListToSort +" "+"There are no elements in the list to check the sort order");
		}
		return isSorted;
}
	
     /**
     * @param i
     * @param byLocator
     */
    public void iteratorClick(int temp, By byLocator){
		
		try {
		    String xpath = byLocator.toString();
		    String var = "'"+temp+"'";
		    xpath =  xpath.replaceAll("__placeholder", var);
		    String[] test = xpath.split(": ");
		    xpath = test[1];
		    iosDrive.findElement(By.xpath(xpath)).click();
		    } catch (Exception e) {
			System.out.println(e);
		}
	}
	
       /**
       * @param i
       * @param byLocator
       * @returns the By locator
       */
     public String iterativeXpathtoStringGenerator(int temp, By byLocator , String property){
    	 
    	 WebElement element =null;
    	 String drug = null;
 		try {
 			
 			String xpath = byLocator.toString();
 			String var = "'"+temp+"'";
 		    xpath = xpath.replaceAll("__placeholder", var);
 		    String[] test = xpath.split(": ");
		    xpath = test[1];
		    element = iosDrive.findElement(By.xpath(xpath));
		    drug = element.getAttribute(property);
 		} catch (Exception e) {
 			System.out.println(e);
 		}
 		return drug;
 	}
		
     /**
     * @param temp
     * @param byLocator
     * @return
     */
    public By iterativeXpathText(String temp, By byLocator){
    	 
    	 By searchResultList = null ;
    	 
 		try {
 			
 			String xpath = byLocator.toString();
 			String var = "'"+temp+"'";
 		    xpath = xpath.replaceAll("__placeholder", var);
 		    String[] test = xpath.split(": ");
		    xpath = test[1];
 		    searchResultList = By.xpath(xpath);
 		} catch (Exception e) {
 			System.out.println(e);
 		}
 		return searchResultList;
 	}
     
 	/**
 	 * @param byLocator
 	 * @returns the list count of the element 
 	 */
 	public int iterativeGetCount(int temp, By byLocator){

 		int count = 0;
 		try {
 			
 			String xpath = byLocator.toString();
		    String var = "'"+temp+"'";
		    xpath =  xpath.replaceAll("__placeholder", var);
		    String[] test = xpath.split(": ");
		    xpath = test[1];
 			count = iosDrive.findElements(By.xpath(xpath)).size();
 			logger.info("List count for" +" "+ xpath +" "+ "is"+" "+ count);	
 			//extent.extentLogger("getCount","List count for" +" "+ xpath +" "+ "is"+" "+ count);
 		} catch (Exception e) {
 			logger.error(e);	
 		}
 		return count;
 	}
     

    /**
     * @param byLocator
     *        Checks whether element is not displayed
     */
    public void checkElementNotPresent(By byLocator){
    	boolean isElementNotPresent = true;
    	 try {
    	  isElementNotPresent = checkcondition(byLocator);
    	  softAssert.assertEquals(isElementNotPresent, false);
          logger.info("The element " + byLocator +" "+ "is not displayed");
		//  extent.extentLogger("checkElementNotPresent" , "The element " + byLocator + "is not displayed");
		  } catch (Exception e) {
		   softAssert.assertEquals(isElementNotPresent, true, "Element" + byLocator +" "+ "is visible");
		   softAssert.assertAll();
		   logger.error("Element" + byLocator +" "+ "is visible");
		  // extent.extentLogger("checkElementNotPresent" , "The element " + byLocator + "is displayed");
		}
    }
    

    /**
     * @param stringOne
     * @param stringTwo
     */
    public void validateTwoStrings(String stringOne, String stringTwo ){
    	
    	if(stringOne.equals(stringTwo)) {    		
    		logger.info("The String " + stringOne +" "+ "matches the other"+" "+stringTwo);
  		  //  extent.extentLogger("validateTwoStrings" , "The String " + stringOne +" "+ "matches the other"+" "+stringTwo);
  		  	
    	} else {
    		softAssert.assertAll();
    		logger.info("The String " + stringOne +" "+ "does not match the other"+" "+stringTwo);
  		  //  extent.extentLogger("validateTwoStrings" , "The String " + stringOne +" "+ "does not match the other"+" "+stringTwo);
  		}
    }
    
    

    
    
    
    
    /**
     * @param byLocator
     */
    public void clearTextField(By byLocator){
    	try {
    		iosDrive.findElement(byLocator).clear();
    	} catch (Exception e) {
		System.out.println(e);
		}
    }
    
    /**
    * Read the test data from excel file
    *
    * @param data The TestData data object
    */

	/*
	 * public void readExcelData (TestData data) {
	 * 
	 * ArrayList<String> username = new ArrayList<String>(); ArrayList<String>
	 * password = new ArrayList<String>(); ArrayList<String> element1 = new
	 * ArrayList<String>(); ArrayList<String> element2 = new ArrayList<String>();
	 * ArrayList<String> element3 = new ArrayList<String>();
	 * 
	 * // Get the data from excel file for (int rowCnt = 1; rowCnt <
	 * UtilityExcel.RowCount(); rowCnt++) {
	 * username.add(UtilityExcel.ReadCell(UtilityExcel.GetCell("User ID"), rowCnt));
	 * password.add(UtilityExcel.ReadCell(UtilityExcel.GetCell("Password"),
	 * rowCnt));
	 * element1.add(UtilityExcel.ReadCell(UtilityExcel.GetCell("Element1"),
	 * rowCnt));
	 * element2.add(UtilityExcel.ReadCell(UtilityExcel.GetCell("Element2"),
	 * rowCnt));
	 * element3.add(UtilityExcel.ReadCell(UtilityExcel.GetCell("Element3"),
	 * rowCnt)); }
	 * 
	 * data.setLoginUser(username); data.setPassword(password);
	 * data.setElement1(element1); data.setElement2(element2);
	 * data.setElement3(element3); }
	 * 
	 */
   
    /**
     * @return true if keyboard is displayed
     * @throws IOException
     */
    public boolean checkKeyboardDisplayed() throws IOException{
    boolean mInputShown = false;	
    try {
    String cmd = "adb shell dumpsys input_method | grep mInputShown";
    Process p = Runtime.getRuntime().exec(cmd);
    BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
    String outputText ="";
    while((outputText = br.readLine()) !=null){
    if(!outputText.trim().equals("")){
    String[] output = outputText.split(" "); 
    String[] value = output[output.length-1].split("=");
    String keyFlag = value[1];
    if(keyFlag.equalsIgnoreCase("True")){
    mInputShown = true;
    }
    }
    }
    br.close();
    p.waitFor();
    } catch (Exception e) {
    System.out.println(e);	
	}	
	return mInputShown;
  	}
    
    /**
     * To read all the data from the CSV file
     * @throws IOException
     */
	/*
	 * public static void readAllDataAtOnce(String file) { try { // Create an object
	 * of file reader // class with CSV file as a parameter. FileReader filereader =
	 * new FileReader(file);
	 * 
	 * // create csvReader object and skip first Line CSVReader csvReader = new
	 * CSVReaderBuilder(filereader) .withSkipLines(1) .build(); List<String[]>
	 * allData = csvReader.readAll();
	 * 
	 * // print Data for (String[] row : allData) { for (String cell : row) {
	 * System.out.print(cell + "\t"); } System.out.println(); } } catch (Exception
	 * e) { e.printStackTrace(); } }
	 */

    

}
	
