package com.Tracfone.driverInstance;

import java.net.URL;

import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.Tracfone.propertyfilereader.PropertyFileReader;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

public class DTAFDriverInstance {

	private String host;
	private int port;
	private int port2;
	private static String deviceName;
	private String platform;
	private static int appTimeOut;
	private String reportDirectory = "reports";
	private String reportFormat = "xml";

	public static AppiumDriver<MobileElement> driver;

	public AndroidDriver<MobileElement> androidDriver;

	/** The IOS driver. */
	public IOSDriver<MobileElement> iosDrive;

	public static AppiumDriver<MobileElement> getDriver(AppiumDriver<MobileElement> driver) {
		System.out.println("GETDriver...........!" + driver);
		DTAFDriverInstance.driver = driver;
		return driver;

	}

	public void setDriver(AppiumDriver<MobileElement> driver) {
		DTAFDriverInstance.driver = driver;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public int getPort2() {
		return port2;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public void setPort2(int port2) {
		this.port2 = port2;
	}

	public static String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		DTAFDriverInstance.deviceName = deviceName;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatfrom(String Platform) {
		this.platform = Platform;
	}

	public static int getappTimeOut() {
		return appTimeOut;
	}

	public void setappTimeOut(int timeOut) {
		DTAFDriverInstance.appTimeOut = timeOut;
	}

	public DTAFDriverInstance(AppiumDriver<MobileElement> driver, String platform) {

		try {
			System.out.println("In DriverInstance!!!!!!");
			System.out.println(driver);

			init();

			// String remoteUrl = "http://" + getHost() + ":" + getPort() + "/wd/hub";
			// String remoteUrl2 = "http://" + getHost() + ":" + getPort2() + "/wd/hub";
			if (platform.equalsIgnoreCase("Android")) {
				DTAFDriverInstance.driver = driver;
				System.out.println("Control in Android Driver Started");
				System.out.println("Driver Started");
				System.out.println(driver);
				getDriver(DTAFDriverInstance.driver);

			} else if (platform.equalsIgnoreCase("iOS")) {
				// driver = new IOSDriver<MobileElement>(new URL(remoteUrl),
				// DTAFDriverInstance.generateiOSCapabilities(Application));
				DTAFDriverInstance.driver = driver;
				System.out.println("Control in iOS Driver Started");
				System.out.println("iOS Driver Started");
				System.out.println(driver);
				getDriver(DTAFDriverInstance.driver);
			} else {
				throw new Exception("Given platform is not implemented.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void tearDown() {
		driver.quit();
	}

	protected void init() {
		PropertyFileReader handler = new PropertyFileReader("properties/Execution.properties");
		setHost(handler.getproperty("HOST_IP"));
		setPort(Integer.parseInt(handler.getproperty("HOST_PORT")));
		setDeviceName(handler.getproperty("DEVICE_NAME"));
		// setUDID(handler.getproperty("UDID"));
		setPlatfrom(handler.getproperty("PLATFORM_NAME"));
		setappTimeOut(Integer.parseInt(handler.getproperty("APP_TIMEOUT")));
	}

	/**
	 * @param application
	 * @return Android capabilities
	 */
	
	
	
	
	public static DesiredCapabilities generateAndroidCapabilities(String application) {

		DesiredCapabilities capabilities = new DesiredCapabilities();

		if (application.equalsIgnoreCase("Tracfone")) {
					
			// PCloudy Samsung Device			
			
			/*
			 * capabilities.setCapability("pCloudy_Username", "suganyak@hcl.com");
			 * capabilities.setCapability("pCloudy_ApiKey", "cfwqp874y2782k763zjg9777");
			 * capabilities.setCapability("pCloudy_DurationInMinutes", 10);
			 * capabilities.setCapability("newCommandTimeout", 600);
			 * capabilities.setCapability("launchTimeout", 90000);
			 * capabilities.setCapability("pCloudy_DeviceFullName",
			 * "SAMSUNG_SamsungGalaxyS9Plus_Android_10.0.0_e46d5");
			 * capabilities.setCapability("platformVersion", "10.0.0");
			 * capabilities.setCapability("platformName", "Android");
			 * capabilities.setCapability("automationName", "uiautomator2");
			 * capabilities.setCapability("pCloudy_ApplicationName",
			 * "myAccount_SimpleMobile-sit-debug.apk");
			 * capabilities.setCapability("appPackage",
			 * "com.tracfone.simplemobile.myaccount");
			 * capabilities.setCapability("appActivity",
			 * "com.tracfone.generic.myaccountcommonui.activity.launch.PreLaunchActivity");
			 * capabilities.setCapability("pCloudy_WildNet", "false");
			 * capabilities.setCapability("pCloudy_EnableVideo", "true");
			 * capabilities.setCapability("pCloudy_EnablePerformanceData", "true");
			 * capabilities.setCapability("pCloudy_EnableDeviceLogs", "true");
			 * capabilities.setCapability("autoAcceptAlerts", true);
			 */
			
			capabilities.setCapability("pCloudy_Username", "suganyak@hcl.com");
			capabilities.setCapability("pCloudy_ApiKey", "bbn4nzybn9gtftzmrsw72pmn");
			capabilities.setCapability("pCloudy_DurationInMinutes", 30);
			capabilities.setCapability("newCommandTimeout", 600);
			capabilities.setCapability("launchTimeout", 90000);
			capabilities.setCapability("pCloudy_DeviceFullName", "SAMSUNG_SamsungGalaxyS9Plus_Android_10.0.0_e46d5");
			capabilities.setCapability("platformVersion", "10.0.0");
			capabilities.setCapability("platformName", "Android");
			capabilities.setCapability("automationName", "uiautomator2");
			capabilities.setCapability("pCloudy_ApplicationName", "myAccount_SimpleMobile-sit-debug.apk");
			capabilities.setCapability("appPackage", "com.tracfone.simplemobile.myaccount");
			capabilities.setCapability("appActivity", "com.tracfone.generic.myaccountcommonui.activity.launch.PreLaunchActivity");
			capabilities.setCapability("pCloudy_WildNet", "false");
			capabilities.setCapability("pCloudy_EnableVideo", "true");
			capabilities.setCapability("pCloudy_EnablePerformanceData", "true");
			capabilities.setCapability("pCloudy_EnableDeviceLogs", "true");
			
			 
				  System.out.println("Control in android caps");
				  System.out.println("Capabilities Set");
				 

		} else {
			// To implement the Logger
		}
		capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, getappTimeOut());
		return capabilities;
	}

		
	
	public static DesiredCapabilities generateiOSCapabilities(String application) {

		DesiredCapabilities capabilities = new DesiredCapabilities();
		if (application.equalsIgnoreCase("Tracfone")) 
		{
			
			//DesiredCapabilities capabilities = new DesiredCapabilities();
			
			/*
			 * capabilities.setCapability("pCloudy_Username", "skrishnan@tracfone.com");
			 * capabilities.setCapability("pCloudy_ApiKey", "b27g22w479qhgryfsskxtrcq");
			 * capabilities.setCapability("pCloudy_DurationInMinutes", 30);
			 * capabilities.setCapability("newCommandTimeout", 600);
			 * capabilities.setCapability("launchTimeout", 90000);
			 * capabilities.setCapability("pCloudy_DeviceFullName",
			 * "APPLE_iPhone12_iOS_14.3.0_83519");
			 * capabilities.setCapability("platformVersion", "14.3.0");
			 * capabilities.setCapability("platformName", "ios");
			 * capabilities.setCapability("acceptAlerts", true);
			 * capabilities.setCapability("automationName", "XCUITest");
			 * capabilities.setCapability("pCloudy_ApplicationName",
			 * "SimpleMobile_Resigned1618478550.ipa");
			 * capabilities.setCapability("bundleId","com.tracfone.Simple-Mobile-My-Account"
			 * );
			 * 
			 * capabilities.setCapability("pCloudy_WildNet", "false");
			 * capabilities.setCapability("pCloudy_EnableVideo", "true");
			 * capabilities.setCapability("pCloudy_EnablePerformanceData", "true");
			 * capabilities.setCapability("pCloudy_EnableDeviceLogs", "true");
			 */
			  
			  
			  
			  capabilities.setCapability("pCloudy_Username", "suganyak@hcl.com");
			  capabilities.setCapability("pCloudy_ApiKey", "bbn4nzybn9gtftzmrsw72pmn");
			  capabilities.setCapability("pCloudy_DurationInMinutes", 20);
			  capabilities.setCapability("newCommandTimeout", 600);
			  capabilities.setCapability("launchTimeout", 90000);
			  capabilities.setCapability("pCloudy_DeviceFullName", "APPLE_iPhoneXS_iOS_13.3.0_395b1");
			  capabilities.setCapability("platformVersion", "13.3.0");
			  capabilities.setCapability("platformName", "ios");
			  capabilities.setCapability("acceptAlerts", true);
			  capabilities.setCapability("automationName", "XCUITest");
			  capabilities.setCapability("pCloudy_ApplicationName", "Simple-Mobile-My-Account_Resigned1618822290.ipa");
			  capabilities.setCapability("bundleId", "com.tracfone.Simple-Mobile-My-Account");
			  capabilities.setCapability("pCloudy_WildNet", "false");
			  capabilities.setCapability("pCloudy_EnableVideo", "true");
			  capabilities.setCapability("pCloudy_EnablePerformanceData", "true");
			  capabilities.setCapability("pCloudy_EnableDeviceLogs", "true");
			  
			 
			//IOSDriver<WebElement> driver = new IOSDriver<WebElement>(new URL("https://tracfone.pcloudy.com/appiumcloud/wd/hub"), capabilities);
			
			
			/*
			 * capabilities.setCapability("pCloudy_Username", "suganyak@hcl.com");
			 * capabilities.setCapability("pCloudy_ApiKey", "cfwqp874y2782k763zjg9777");
			 * capabilities.setCapability("pCloudy_DurationInMinutes", 20);
			 * capabilities.setCapability("newCommandTimeout", 600);
			 * capabilities.setCapability("launchTimeout", 90000);
			 * capabilities.setCapability("pCloudy_DeviceFullName",
			 * "APPLE_iPhoneXSMax_iOS_12.0.0_b923a");
			 * capabilities.setCapability("platformVersion", "12.0.0");
			 * capabilities.setCapability("platformName", "ios");
			 * capabilities.setCapability("acceptAlerts", true);
			 * capabilities.setCapability("automationName", "XCUITest");
			 * capabilities.setCapability("pCloudy_ApplicationName",
			 * "Simple-Mobile-My-Account_Resigned1618822290.ipa");
			 * capabilities.setCapability("bundleId",
			 * "com.tracfone.Simple-Mobile-My-Account");
			 * capabilities.setCapability("pCloudy_WildNet", "false");
			 * capabilities.setCapability("pCloudy_EnableVideo", "true");
			 * capabilities.setCapability("pCloudy_EnablePerformanceData", "true");
			 * capabilities.setCapability("pCloudy_EnableDeviceLogs", "true");
			 * capabilities.setCapability("noReset", true);
			 */
			
			System.out.println("iOS Capabilities Set");
			System.out.println("Control in iOS caps");
			
			
			
		} else {
			// To implement the Logger
		}
		capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, getappTimeOut());
		return capabilities;
	}

}