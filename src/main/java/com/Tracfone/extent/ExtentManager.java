package com.Tracfone.extent;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class ExtentManager {
    private static ExtentReports extent;
   
    private static String time = getDate();
    private static String reportFileName = "SimplyMobile Automation Report"+".html";
    //private static String fileSeperator = System.getProperty("file.separator");
    //private static String reportFilepath = System.getProperty("user.dir") +fileSeperator+ "TestReport_NewExtent";
    //private static String reportFileLocation =  reportFilepath +fileSeperator+ reportFileName;
    //ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/Reports" + "/" + reportname + "/" + "/" + reportname + "_" + time + "/" + reportname + "_" + time + ".html");
 
    public static ExtentReports getInstance() {
        if (extent == null)
            createInstance();
        return extent;
    }
 
    //Create an extent report instance
    public static ExtentReports createInstance() {
        //String fileName = getReportPath(reportFilepath);
       String reportname = "Tracfone";
        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/Reports_New" + "/" + reportname + "/" + "/" + reportname + "_" + time + "/" + reportname + "_" + time + ".html");
        htmlReporter.config().setTheme(Theme.STANDARD);
        htmlReporter.config().setDocumentTitle(reportFileName);
        htmlReporter.config().setEncoding("utf-8");
        htmlReporter.config().setReportName(reportFileName);
        htmlReporter.config().setTimeStampFormat("EEEE, MMMM dd, yyyy, hh:mm a '('zzz')'");
 
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
        
        //Set environment details
		extent.setSystemInfo("OS", "Windows");
		extent.setSystemInfo("Environment", "Test");
 
        return extent;
        
       

    }
     
    //Create the report path
	/*
	 * private static String getReportPath (String path) { File testDirectory = new
	 * File(path); if (!testDirectory.exists()) { if (testDirectory.mkdir()) {
	 * System.out.println("Directory: " + path + " is created!" ); return
	 * reportFileLocation; } else {
	 * System.out.println("Failed to create directory: " + path); return
	 * System.getProperty("user.dir"); } } else {
	 * System.out.println("Directory already exists: " + path); } return
	 * reportFileLocation; }
	 */
    public static String getDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String name = dateFormat.format(date).toString().replaceFirst(" ", "_").replaceAll("/", "_").replaceAll(":","_");
		return name;
	}
    
	/*
	 * public String getScreenShot(AppiumDriver<MobileElement> driver, String
	 * screenshotName) throws IOException { this.driver = driver; String dateName =
	 * new SimpleDateFormat("yyyyMMddhhmmss").format(new Date()); TakesScreenshot ts
	 * = (TakesScreenshot) this.driver; File source =
	 * ts.getScreenshotAs(OutputType.FILE); // after execution, you could see a
	 * folder "FailedTestsScreenshots" under src folder String destination
	 * =System.getProperty("user.dir")+ "/Screenshots/" + screenshotName + dateName
	 * + ".png"; File finalDestination = new File(destination);
	 * FileUtils.copyFile(source, finalDestination); return destination; }
	 */
	
 
}