package com.Tracfone.mobileobjects;

import org.openqa.selenium.By;

import io.appium.java_client.MobileBy;

public class Objects_iOS {
	
	public static By allowButton = By.xpath("//XCUIElementTypeButton[@name='Allow']");
	public static By agreeButton = By.xpath("//XCUIElementTypeButton[@name='Agree']");
	public static By endpointLink = By.xpath("//XCUIElementTypeButton[@name='Choose Endpoint']");
	public static By endpointTextBox = By.xpath("//XCUIElementTypeApplication[@name='My Account']/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField");
    public static By applyButton = By.xpath("//XCUIElementTypeButton[@name='Apply']");
    public static By backButton = By.xpath("//XCUIElementTypeButton[@name='My Account']");
    public static By menuButton = By.xpath("//XCUIElementTypeButton[@name='Menu']");
    public static By activateDevice = By.xpath("//XCUIElementTypeCell[@name='Activate Device']");
    public static By simpleMobilePhone = By.xpath("//XCUIElementTypeStaticText[@name='Simple Mobile Phone']");
    public static By deviceTypeLabel = By.xpath("//XCUIElementTypeStaticText[@name='Device Type']");
    public static By simcardMessage = MobileBy.AccessibilityId("I already have a Simple Mobile compatible SIM card");
    public static By simcardnumber = MobileBy.AccessibilityId("SIM Card Number");
    public static By termscondition = By.xpath("//XCUIElementTypeButton[@name='Please accept the Terms & Conditions before continuing.']");
    public static By continueButton = By.xpath("//XCUIElementTypeButton[@name='CONTINUE']");
    public static By ownDevice = MobileBy.AccessibilityId("Keep Your Own Device");
    public static By activationTypeLabel = By.xpath("//XCUIElementTypeStaticText[@name='Activation Type']");
    public static By keepphoneNumber = By.xpath("//XCUIElementTypeButton[@name='KEEP PHONE NUMBER']");
    public static By newPhoneNumber = By.xpath("//XCUIElementTypeButton[@name='NEW PHONE NUMBER']");
    public static By newPhonenumberLabel = By.xpath("//XCUIElementTypeStaticText[@name='New Phone Number']");
    public static By zipcode = MobileBy.AccessibilityId("Zip Code");
    public static By continueNewPhone = By.xpath("//XCUIElementTypeButton[@name='CONTINUE']");
    public static By servicePlanLabel = By.xpath("//XCUIElementTypeNavigationBar[@name='Service Plan']");
    public static By purchasewithPin = By.xpath("//XCUIElementTypeButton[@name='ADD SERVICE PLAN WITH A PIN']");
    public static By enterPinLabel = By.xpath("//XCUIElementTypeStaticText[@name='Enter PIN']");
    public static By pinText = MobileBy.AccessibilityId("Pin Number");
    public static By createButton = MobileBy.AccessibilityId("Create");
    public static By registerPageLabel = By.xpath("//XCUIElementTypeNavigationBar[@name='Register']");
    public static By emailId = MobileBy.AccessibilityId("Email");
    public static By confirmemailId = MobileBy.AccessibilityId("Confirm Email");
    public static By password = MobileBy.AccessibilityId("Password");
    public static By confirmPassword = MobileBy.AccessibilityId("Confirm Password");
    public static By securityPin = MobileBy.AccessibilityId("Security PIN");
    public static By confirmSecurityPin = MobileBy.AccessibilityId("Confirm Security PIN");
    public static By DOBM = MobileBy.AccessibilityId("Month");
    public static By DOBD = MobileBy.AccessibilityId("Date");
    public static By DOBY = MobileBy.AccessibilityId("Year");
    public static By saveAccount = By.xpath("//XCUIElementTypeButton[@name='SAVE ACCOUNT']");
}