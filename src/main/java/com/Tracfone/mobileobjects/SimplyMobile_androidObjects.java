package com.Tracfone.mobileobjects;

import org.openqa.selenium.By;

import io.appium.java_client.MobileBy;

public class SimplyMobile_androidObjects {
	
	public static By welcome_Text = By.id("com.tracfone.simplemobile.myaccount:id/heading");
	public static By remindLater = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.Button[2]");
	public static By continueButton = By.id("com.tracfone.simplemobile.myaccount:id/agree_permissions_btn");
	public static By phonePermission = By.xpath("//*[@text='Allow']");
	public static By locationPermission = By.id("com.tracfone.simplemobile.myaccount:id/agree_permissions_btn");
	public static By phonePermissionDeny = By.xpath("//*[@text='Deny']");
	public static By endUserAgreement = By.id("com.tracfone.simplemobile.myaccount:id/agree_button");
	public static By endPointText = By.id("com.tracfone.simplemobile.myaccount:id/String_EditUrl");
	public static By acceptButton = By.id("com.tracfone.simplemobile.myaccount:id/agree_button");
	
	public static By logInPageTitle = By.xpath("//android.widget.TextView[@content-desc='Log In']");
	public static By menuButton = By.xpath("//android.widget.ImageButton[@content-desc='menu']");
	public static By addDevice = By.xpath("//*[@text='Add Device or Line']");
	public static By activateDevice = By.id("com.tracfone.simplemobile.myaccount:id/active_device_login_screen");
	public static By devicePageHeading = By.id("com.tracfone.simplemobile.myaccount:id/toolbar_title");
	public static By bringOwnDevice = By.xpath("//*[@text='Bring Your Own Device']");
	public static By simCardField = By.xpath("//android.widget.EditText[@content-desc='Enter SIM Card Number']");
	public static By checkBox = By.id("com.tracfone.simplemobile.myaccount:id/terms_conditions_ckbox");
	public static By simCardContinue = By.id("com.tracfone.simplemobile.myaccount:id/continue_btn_deviceType");
	public static By activationTypeTitle= By.xpath("//android.widget.TextView[@content-desc='Activation Type']");
	public static By newPhoneNumber = By.id("com.tracfone.simplemobile.myaccount:id/newPhonenumber_btn");
	public static By newPhoneNumberTitle  = By.xpath("//android.widget.TextView[@content-desc='New Phone Number']");
	public static By zipCodeField = By.id("com.tracfone.simplemobile.myaccount:id/collect_edt");
	public static By zipCodeContinue = By.id("com.tracfone.simplemobile.myaccount:id/zip_continue_btn");
	public static By servicePlanTitle = By.xpath("//android.widget.TextView[@content-desc='Service Plan']");
	public static By servicePlanWithPin = By.id("com.tracfone.simplemobile.myaccount:id/add_airtime_pin_btn");
	public static By enterPinTitle = By.xpath("//android.widget.TextView[@content-desc='Enter PIN']");
	public static By servicePinField = By.id("com.tracfone.simplemobile.myaccount:id/pin_number_edt");
	public static By servicePinContinue = By.id("com.tracfone.simplemobile.myaccount:id/continue_pin_btn");
	public static By createAccount = By.id("com.tracfone.simplemobile.myaccount:id/custom_dialog_body_btn_left");
	
	public static By createAccountTitle= By.xpath("//android.widget.TextView[@content-desc='Create Account']"); 
	public static By enterEmail = By.id("com.tracfone.simplemobile.myaccount:id/et_new_account_email");
	public static By confirmEmail = By.id("com.tracfone.simplemobile.myaccount:id/et_new_account_email_confirm");
	public static By closeButton = By.id("com.samsung.android.samsungpassautofill:id/close_icon");
	public static By password = By.id("com.tracfone.simplemobile.myaccount:id/et_new_account_password");
	public static By confirmPassword = By.id("com.tracfone.simplemobile.myaccount:id/et_new_account_password_confirm");
	public static By pin = By.id("com.tracfone.simplemobile.myaccount:id/et_new_account_pin");
	public static By confirmPin = By.id("com.tracfone.simplemobile.myaccount:id/et_new_account_pin_confirm");
	public static By dateOfBirth = By.id("com.tracfone.simplemobile.myaccount:id/Et_CA_DOB");
	public static By calenderWidget = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.ImageView");
	public static By year = By.id("com.tracfone.simplemobile.myaccount:id/year_display");
	
	public static By setButton = By.id("com.tracfone.simplemobile.myaccount:id/SetDateTime");
	public static By saveAccount = By.id("com.tracfone.simplemobile.myaccount:id/save_info_btn");
	public static By okButtonAccountCreated = By.xpath("//*[@text='OK']");
	public static By deviceConfigurationTitle= By.xpath("//android.widget.TextView[@content-desc='DEVICE CONFIGURATION']");
	public static By continueButtonDC = By.id("com.tracfone.simplemobile.myaccount:id/continue_instructions_btn");
	
	public static By transactionSummaryTitle= By.xpath("//android.widget.TextView[@content-desc='Transaction Summary']");
	public static By doneButton = By.id("com.tracfone.simplemobile.myaccount:id/doneButton");
	public static By dismissButton = By.id("com.tracfone.simplemobile.myaccount:id/popOverButtonOne");
	
	public static By myAccountSummaryTitle= By.xpath("//android.widget.TextView[@content-desc='My Account Summary']");
	public static By simNumber = By.id("com.tracfone.simplemobile.myaccount:id/device_nickname");
	
	
}