package com.Database_ReusableMethods;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.CallableStatement;
public class SQLDatabaseConnection {
   
	// Connection to database.
    // Replace server name, username, and password with credentials
    public static String DBConnection() throws ClassNotFoundException, SQLException {
    	String entryStatus = null;
    	System.out.println("In DB Connection Method..!!!!!");
		Class.forName("oracle.jdbc.OracleDriver");  
		Connection con=DriverManager.getConnection("jdbc:oracle:thin:@devdb.tracfone.com:3011:CLFYSIT1","itquser_data","sit14testdata");  
		System.out.println("DB Connection is Established");
		//Statement stmt=con.createStatement();  
		//System.out.println("The Statement is" +stmt);
		String query = "set serveroutput on;" 
				+"DECLARE" 
				+ "v_branded_flag VARCHAR2(1) := 'N'; -- Y or N"  
				+"v_esn_pn VARCHAR2(20) := 'PHSM128PSIMT5TODP';-------TWSAG960U1CP" 
				+ "v_sim_pn VARCHAR2(20) := 'SM128PSIMT5TODP';------TF256PSIMV97N" 
				+ "v_pin_pn VARCHAR2(20) := 'SMNAPP0050BBUNL';------TWAPP00060"  
				+"v_esn VARCHAR2(20) := '';" 
				+ "v_sim VARCHAR2(20) := '';" + 
				"v_pin VARCHAR2(20) := '';" + 
				"BEGIN\r\n" + 
				"-----DBMS_OUTPUT.PUT_LINE('------------------ DB OUTPUT --------------------');\r\n" + 
				"IF v_branded_flag = 'Y'" + 
				"THEN --{" + 
				"v_esn := GET_TEST_ESN(v_esn_pn);" + 
				"v_sim := GET_TEST_SIM(v_sim_pn);" + 
				 "UPDATE table_part_inst" + 
				"SET x_iccid = v_sim" + 
				"WHERE part_serial_no = v_esn;" + 
				"COMMIT;" + 
				"DBMS_OUTPUT.PUT_LINE('----- BRANDED ESN, SIM AND PIN -----');" + 
				"ELSE --}{" + 
				"v_esn := SA.GET_TEST_ESN_BYOP(v_esn_pn,v_sim_pn);" + 
				"select x_iccid into v_sim from table_part_inst where part_serial_no = v_esn;" + 
				"--DBMS_OUTPUT.PUT_LINE('v_sim '||v_sim );" + 
				"DBMS_OUTPUT.PUT_LINE('----- BYOP PSEDO ESN, SIM AND PIN -----');" + 
				"END IF; --}" + 
				"v_pin := GET_TEST_PIN(v_pin_pn);" + 
				"DBMS_OUTPUT.PUT_LINE('ESN: '||v_esn);" + 
				"DBMS_OUTPUT.PUT_LINE('SIM: '||v_SIM);" + 
				"DBMS_OUTPUT.PUT_LINE('PIN: '||v_PIN);" + 
				"END;";
		
		CallableStatement cs = con.prepareCall(query);
		if (cs.equals(null)) {
			entryStatus = "No DB record found";
			System.out.println("************************************************");
			System.out.println(entryStatus);
			System.out.println("************************************************");
		} 
		else 
		{
			entryStatus = "DB record found";
			System.out.println(entryStatus);
			
			
}
		return(entryStatus);
    }
}