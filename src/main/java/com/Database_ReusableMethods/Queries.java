package com.Database_ReusableMethods;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.commons.exec.ExecuteException;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.Database_ReusableMethods.SQLDatabaseConnection;
import com.Tracfone.driverInstance.DTAFCommandBase_Android;
import com.Tracfone.driverInstance.DTAFDriverInstance;
import com.Tracfone.excel.*;
import com.Tracfone.extent.ExtentReporter;
import com.Tracfone.extent.ExtentTestManager;
import com.Tracfone.propertyfilereader.PropertyFileReader;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import jxl.read.biff.BiffException;


public class Queries {

	
	public static SQLDatabaseConnection _db;
	
	public static void getAccountNumberSSN() throws ExecuteException, IOException, InterruptedException, BiffException
	{
				  try
		  {
			  // Call DB Connection establishment 
					  SQLDatabaseConnection.DBConnection();
			  //SQLDatabaseConnection.ExecuteQuery("");
		  }
		  catch (Exception e) {
				e.printStackTrace();
				}
	}
	
	
	public static void getESNSIMandPIN()
	{
		try
		{
			SQLDatabaseConnection.DBConnection();
			/*
			 * SQLDatabaseConnection.ExecuteQuery("set serveroutput on;\r\n" + "\r\n" +
			 * "DECLARE\r\n" + "\r\n" + "v_branded_flag VARCHAR2(1) := 'Y'; -- Y or N\r\n" +
			 * "\r\n" +
			 * "v_esn_pn       VARCHAR2(20) := 'TWSAG960U1CP';-------TWSAG960U1CP\r\n" +
			 * "\r\n" +
			 * "v_sim_pn       VARCHAR2(20) := 'TF256PSIMV97N';------TF256PSIMV97N\r\n" +
			 * "\r\n" + "v_pin_pn       VARCHAR2(20) := 'TWAPP00060';------TWAPP00060\r\n" +
			 * "\r\n" + "v_esn          VARCHAR2(20) := '';\r\n" + "\r\n" +
			 * "v_sim          VARCHAR2(20) := '';\r\n" + "\r\n" +
			 * "v_pin          VARCHAR2(20) := '';\r\n" + "\r\n" + "BEGIN\r\n" + "\r\n" +
			 * "-----DBMS_OUTPUT.PUT_LINE('------------------ DB OUTPUT --------------------');\r\n"
			 * + "\r\n" + "IF v_branded_flag = 'Y'\r\n" + "\r\n" + "THEN --{\r\n" + "\r\n" +
			 * "v_esn := GET_TEST_ESN(v_esn_pn);\r\n" + "\r\n" +
			 * "v_sim := GET_TEST_SIM(v_sim_pn);\r\n" + "\r\n" +
			 * " UPDATE table_part_inst\r\n" + "\r\n" + "SET    x_iccid = v_sim\r\n" +
			 * "\r\n" + "WHERE  part_serial_no = v_esn;\r\n" + "\r\n" + "COMMIT;\r\n" +
			 * "\r\n" + " \r\n" + "\r\n" +
			 * "DBMS_OUTPUT.PUT_LINE('----- BRANDED ESN, SIM AND PIN -----');\r\n" + "\r\n"
			 * + " \r\n" + "\r\n" + "ELSE --}{\r\n" + "\r\n" + " \r\n" + "\r\n" +
			 * "v_esn := SA.GET_TEST_ESN_BYOP(v_esn_pn,v_sim_pn);\r\n" + "\r\n" +
			 * "select x_iccid into v_sim from table_part_inst where part_serial_no = v_esn;\r\n"
			 * + "\r\n" + "--DBMS_OUTPUT.PUT_LINE('v_sim  '||v_sim );\r\n" + "\r\n" +
			 * "DBMS_OUTPUT.PUT_LINE('----- BYOP PSEDO ESN, SIM AND PIN -----');\r\n" +
			 * "\r\n" + "END IF; --}\r\n" + "\r\n" + "v_pin := GET_TEST_PIN(v_pin_pn);\r\n"
			 * + "\r\n" + " \r\n" + "\r\n" + "DBMS_OUTPUT.PUT_LINE('ESN: '||v_esn);\r\n" +
			 * "\r\n" + "DBMS_OUTPUT.PUT_LINE('SIM: '||v_SIM);\r\n" + "\r\n" +
			 * "DBMS_OUTPUT.PUT_LINE('PIN: '||v_PIN);\r\n" + "\r\n" + "END;\r\n" + "");
			 */
		}
		
		catch (Exception e) {
			e.printStackTrace();
			}
		}
	
	
	
	}
