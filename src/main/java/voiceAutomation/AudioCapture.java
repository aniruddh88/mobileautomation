package voiceAutomation;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.testng.annotations.Test;
import me.xdrop.fuzzywuzzy.FuzzySearch;
import org.zeroturnaround.exec.ProcessExecutor;
public class AudioCapture {
   
    @Test
    public void testAudioCapture() throws Exception {
        
    	//  WebDriverWait wait = new WebDriverWait(driver, 10);
        // start an ffmpeg audio capture of system audio. Replace with a path and device id
        //Script for capturing the audio from the device directly
    	
        File audioCapture = new File("C:\\Users\\suganyak\\Desktop\\input.wav");
        
        // Specifying the audio duration
        //captureForDuration(audioCapture, 10000);
        System.out.println("Verifying whether the Audio exists..!");
        
        assert(audioCapture.exists());
        
        Thread.sleep(100);
        System.out.println("Audio File in .wav format exists..!!!");
        
       // convertAudio();
        audioComparison();
        
    }

   /* 
    * 
    * This method will convert the audio format as specified
    * 
    */
    
    public void convertAudio() throws IOException 
    {
    	 Process proc;
    	 ArrayList<String> cmd = new ArrayList<>();
         cmd.add("ffmpeg");       // binary should be on path
         cmd.add("-i");           // always overwrite files
         cmd.add("C:\\Users\\suganyak\\Desktop\\input.wav");
         cmd.add("C:\\Users\\suganyak\\Desktop\\outputNew.avi");
         // input
         // device id returned by ffmpeg list
         

         ProcessBuilder pb = new ProcessBuilder(cmd);
         pb.redirectErrorStream(true);
         pb.redirectOutput(Redirect.PIPE);
         StringJoiner out = new StringJoiner("\n");
         try {
             proc = pb.start();
             try (BufferedReader reader = new BufferedReader(
                 new InputStreamReader(proc.getInputStream()))) {

                 reader.lines().forEach(out::add);
             }
             proc.waitFor();
         } 
         catch (InterruptedException ign) {}
         System.out.println("FFMpeg output was: " + out.toString());
    }
    
  /*
   * This method is to validate whether both the audio matches or not 
   * @throws Exception
   *
   */
    
    public void audioComparison() throws Exception 
    {
    	  File audioCapture = new File("C:\\Users\\suganyak\\Desktop\\voicerecording1.wav");
         
    	  File audioCompare = new File("C:\\Users\\suganyak\\Desktop\\voicerecording3.wav");
          
    	  //calculate the fingerprint of the freshly-captured audio...
    	  
    	  System.out.println("Calculating the Fingerprint for 1st audio...");
          AudioFingerprint fp1 = AudioFingerprint.calcFP(audioCapture);

          // Fingerprint calculation of our baseline audio...
          System.out.println("Calculating the Fingerprint for 2nd audio...");
          AudioFingerprint fp2 = AudioFingerprint.calcFP(audioCompare);

          // Audio Comparison
          System.out.println("Comparing both the audios.!");
          double comparison = fp1.compare(fp2);

         
          
          // Verifying that the comparison is Strong
          
          if(comparison > 90.00) {
        	
        	  System.out.println("Both Audios Match..!!  The match % is :" +comparison );
          }
          
          else {
        	 
        	  System.out.println("Both Audio does not Match..!! The match % is :" +comparison );
        	  
          }
         
      }
  }

 
class AudioFingerprint {

      private static String FPCALC = "C:\\chromaprint-fpcalc-1.5.0-windows-x86_64\\chromaprint-fpcalc-1.5.0-windows-x86_64\\fpcalc";

      private String fingerprint;

      AudioFingerprint(String fingerprint) {
          this.fingerprint = fingerprint;
      }

      public String getFingerprint() { return fingerprint; }

      public double compare(AudioFingerprint other) {
          return FuzzySearch.partialRatio(this.getFingerprint(), other.getFingerprint());
      }

      public static AudioFingerprint calcFP(File wavFile) throws Exception {
          String output = new ProcessExecutor()
              .command(FPCALC, "-raw", wavFile.getAbsolutePath())
              .readOutput(true).execute()
              .outputUTF8();

          Pattern fpPattern = Pattern.compile("^FINGERPRINT=(.+)$", Pattern.MULTILINE);
          Matcher fpMatcher = fpPattern.matcher(output);

          String fingerprint = null;

          if (fpMatcher.find()) {
              fingerprint = fpMatcher.group(1);
          }

          if (fingerprint == null) {
              throw new Exception("Could not get fingerprint via Chromaprint fpcalc");
          }

          return new AudioFingerprint(fingerprint);
      }
    }
    
